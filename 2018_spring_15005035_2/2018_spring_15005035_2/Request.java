package buaaoo;

public class Request {	
	
	//请求指令发出的时间
	private int time; 
	
	//指令的类型   电梯内部指令 true ，  楼层指令false 
	private boolean ins_type;  
	
	 //只有楼层指令才有初始化的权限  
	private Floor floor = null; 

	//只有内部指令才有初始化的权限
	private int goal_floor = -1; 
	
	public Floor getFlo() {
		return this.floor;
	}
		
	public int getTime() {
		return this.time;
	}
	
	public boolean getType() {
		return this.ins_type;
	}
	
	public void setTime(int time) {
		this.time = time;
	}
	
	//得到目标楼层函数
	public int get_goal_flo() {
		return this.goal_floor;
	}
	
	//构造函数
	public Request(String ins, boolean istype) {
		String []strs;
		strs = ins.split(",|\\(|\\)");		
		ins_type = istype;
		
		//内部指令
		if(ins_type) {
			int num = Integer.parseInt(strs[2]);
			this.time = Integer.parseInt(strs[3]);
			goal_floor = num;
		}else {
			//楼层指令
			int num = Integer.parseInt(strs[2]);
			String dir = strs[3];
			this.floor = new Floor(num, dir);
			this.time = Integer.parseInt(strs[4]);			
		}
	}
				
}
