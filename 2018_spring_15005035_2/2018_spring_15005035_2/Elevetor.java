package buaaoo;

public class Elevetor {
	
	//运动状态(运动为true， 静止为false)
	private boolean motion = false; 
	
	//电梯当前的运动方向(向上为true，向下为false)
	private boolean direction = true;  	

	//当前所在楼层
	private int flo_num = 1;
	
	public int getFloor() {
		return this.flo_num;
	}
	
	public boolean getDir() {
		return this.direction;
	}
	
	//修改电梯运行后的方向
	public void set_dir_ele(boolean dir_ele) {
		this.direction = dir_ele;
	}
	
	//修改电梯运行后的状态
	public void set_motion_ele(boolean motion_ele) {
		this.motion = motion_ele;
	}
	
	//修改电梯运动后所在楼层
	public void set_flo_ele(int flo_ele) {
		this.flo_num  = flo_ele;
	}
	
	public Elevetor(boolean ele_dir, int _ele_flo) {
		this.direction = ele_dir;
		this.flo_num = _ele_flo;
		this.motion  = false;
	}
}
