package buaaoo;

import java.util.LinkedList;
import java.util.Queue;

import jdk.nashorn.internal.ir.annotations.Ignore;

public class Scheduler {
	
	//设定的标准时间
	private double standard_time = 0;	
	
	private Queue<Request> requests = new LinkedList<Request>();
	
	private Queue<resultOutput> _result = new LinkedList<resultOutput>();	
	
	//电梯类
	private Elevetor elevetor;
	
	public Scheduler(Queue<Request> tmp) {
		requests = tmp;		
	}	
	
	//输出结果
	public class resultOutput {
		private double result_time;
		private String dir;
		private int floor;			
		public resultOutput(double _time , int _flo, String _dir) {
			this.result_time = _time;
			this.floor = _flo;
			this.dir = _dir;				
		}
		
	}
	
	public void Output_req() {
		if(_result.isEmpty()) {
			System.out.println("(1,STILL,0.0)");
		}else {
			while(!_result.isEmpty()) {
				resultOutput rOutput = _result.peek();
				if((rOutput.floor == 1)) {
					System.out.println("(1,STILL,"+rOutput.result_time+")");
				}else {
					System.out.println("("+rOutput.floor+","+rOutput.dir+","+rOutput.result_time+")");	
				}
				_result.poll();
				
				
				while(!_result.isEmpty()) {
					resultOutput rOutput2 = _result.peek();
					if((rOutput2.floor == 1)) {
						System.out.println("(1,STILL,"+rOutput2.result_time+")");
					}else {
						if(rOutput.dir == rOutput2.dir) {
							System.out.println("("+rOutput2.floor+",STILL,"+rOutput2.result_time+")");
						}else {
							System.out.println("("+rOutput2.floor+","+rOutput2.dir+","+rOutput2.result_time+")");	
						}
						rOutput = rOutput2;
					}
					_result.poll();
				}
			}
		}
		
	}
	
	//调度函数
	public void _Schedule_req() {
		
		//过滤队列(过滤同质请求)
		Queue<Request> fliter_req = new LinkedList<Request>();
		elevetor = new Elevetor(false, 1);
		//输出队列
		Request req = null;
		while(!requests.isEmpty() || !fliter_req.isEmpty()) {
			
			boolean flag = false; 
			if(!requests.isEmpty() && fliter_req.isEmpty()){
				req = requests.element();//调度产生的新指令				
			}else {
				if(!fliter_req.isEmpty()) {
					req = fliter_req.element();
					flag = true;
				}else {
					System.out.println("#队列已空");
				}
			}
			
			//内部指令
			if(req.getType()) {
				if(_result.isEmpty()) {  
					//意味着第一条指令是内部指令						
					int tmp = req.get_goal_flo();
					if(tmp == elevetor.getFloor()) {
						//同层请求 执行一次开关门  电梯的其他行为否不会发生改变
						this.standard_time += 1;						
					}else {
						this.standard_time += (tmp - 1)* 0.5 + 1;
						elevetor.set_dir_ele(true);
						elevetor.set_flo_ele(tmp);
					}
					resultOutput res = new resultOutput(this.standard_time, req.get_goal_flo(), "UP");
					_result.add(res);	
					requests.poll();				
				}else {						
							int tmp = req.get_goal_flo();
							int time = req.getTime();
							standard_time = (time > standard_time) ? time:standard_time ;   //电梯两条指令之间的等待时间
							if(tmp > elevetor.getFloor()) {
								this.standard_time += (tmp - elevetor.getFloor())*0.5 + 1;
								elevetor.set_dir_ele(true);
								elevetor.set_flo_ele(tmp);								
							}else if(tmp < elevetor.getFloor()){
								this.standard_time += (elevetor.getFloor() - tmp)*0.5 + 1;
								elevetor.set_dir_ele(false);
								elevetor.set_flo_ele(tmp);
							}else {
								this.standard_time += 1;
							}
							if(elevetor.getDir()) {
								resultOutput res = new resultOutput(this.standard_time, req.get_goal_flo(), "UP");
								_result.add(res);						
							}else {
								resultOutput res = new resultOutput(this.standard_time, req.get_goal_flo(), "DOWN");
								_result.add(res);						
							}							
							if(flag) {
								fliter_req.poll();
							}else {
								requests.poll();
							}
								
					
				}
				
				//同质过滤
				Request r2 = null;
				while(!requests.isEmpty() && requests.element().getTime() <= this.standard_time) {				
					r2 = requests.element();
					fliter_req.add(r2);
					requests.poll();
					}
				int num_filter = fliter_req.size(), i=0;
				while(i<num_filter) {
						r2 = fliter_req.element();
						if((r2.getType()) && (r2.get_goal_flo() == elevetor.getFloor()) ) {
								System.out.println("#same req");
								fliter_req.poll();   
						}else {
								fliter_req.add(r2);					
						}				
					i++;
					}//结束过滤
				
				
			}else {
				//楼层指令				 
				if(_result.isEmpty()) {
					//表示第一条指令是楼层指令
					int tmp  = req.getFlo().getFloor();
					if(tmp == elevetor.getFloor()) { 
						this.standard_time += 1;
					}else {
						this.standard_time += ( tmp -1 ) * 0.5 + 1;
						elevetor.set_dir_ele(true);
						elevetor.set_flo_ele(tmp);
					}
					resultOutput res = new resultOutput(this.standard_time, req.getFlo().getFloor(), "UP");
					_result.add(res);
					requests.poll();
				}else {
						//第一条肯定不是同质指令						
						int tmp = req.getFlo().getFloor();
						int time = req.getTime();
						standard_time = (time > standard_time) ? time:standard_time ;   //电梯两条指令之间的等待时间
						if(elevetor.getFloor() < tmp) {
							this.standard_time += (tmp - elevetor.getFloor())*0.5 + 1;
							elevetor.set_dir_ele(false);
							elevetor.set_flo_ele(tmp);
						}else if(elevetor.getFloor() > tmp){
							this.standard_time += (elevetor.getFloor() - tmp)*0.5 + 1;  
							elevetor.set_dir_ele(true);
							elevetor.set_flo_ele(tmp);
						}else {
							//同层请求    电梯的属性和上次的属性一样
							this.standard_time += 1;	
							if(req.getFlo().getDir()) {
								elevetor.set_dir_ele(true);
							}else {
								elevetor.set_dir_ele(false);
							}
					
						}	
						if(elevetor.getDir()) {
							resultOutput res = new resultOutput(this.standard_time, req.getFlo().getFloor(), "UP");
							_result.add(res);						
						}else {
							resultOutput res = new resultOutput(this.standard_time, req.getFlo().getFloor(), "DOWN");
							_result.add(res);						
						}
						if(flag) {
							fliter_req.poll();
						}else {
							requests.poll();
						}										
				}			
			
			
			//同质过滤
			Request r2 = null;
			while(!requests.isEmpty() && requests.element().getTime() <= this.standard_time) {				
				r2 = requests.element();
				fliter_req.add(r2);
				requests.poll();
				}
			int num_filter = fliter_req.size(), i=0;
			while(i<num_filter) {
					r2 = fliter_req.element();
					if((!r2.getType()) && (r2.getFlo().getFloor() == elevetor.getFloor()) 
								  && (elevetor.getDir() == r2.getFlo().getDir())) {
							System.out.println("#same req");
							fliter_req.poll();   
					}else {						
					fliter_req.add(r2);	
					fliter_req.poll();
					}				
				i++;
				}//结束过滤
			
			}//else
			
		}//while
	}//调度函数
	
	
}
