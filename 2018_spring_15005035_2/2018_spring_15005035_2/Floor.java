package buaaoo;
public class Floor {
	
	//指令发出的楼层号
	private int floor;
	
	//请求的电梯方向(向上为true，向下为false)
	private boolean direction_req;  
	
	public int getFloor() {
		return this.floor;
	}
	
	public boolean getDir() {
		return this.direction_req;
	}
	
	public Floor(int num, String dir) {
		floor = num;
		if(dir.equals("UP")) {
			direction_req = true;
		}else if (dir.equals("DOWN")) {
			direction_req = false;
		}	
		
	}
		
	public Floor() {
		
	}
}

	

