package buaaoo;

import java.util.LinkedList;
import java.util.Queue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RequestQueue {
	

	//最终的命令队列
	private Queue<Request> request_que = new LinkedList<Request>();
	
	//构造函数
	public RequestQueue() {
		
	}
	
	//判断指令的类型
	public int isType(String str) {
			int comma = 0;
			int i=0; 
			while(i<str.length()) {
				if(str.substring(i,i+1).equals(",")) {
					comma++;
				}
				i++;
			}		
			return comma;
		}
	
	//返回队列元素
	public Queue<Request> get_que() {
			return this.request_que;
	}
	
	//判断时间冲突（没有返回true）注意只有相同类型的指令之间才会相同时间的说法
	public boolean isTimeConflict(int time) {
			boolean is = true;
			int _time = 0;
			if(this.request_que.isEmpty()) {
				is = true;
			}else {//必须是时间相同的命令
				for(int i=0; i<this.request_que.size(); i++) {
					Request request = request_que.element();
					request_que.poll();
					request_que.add(request);
					_time = request.getTime();
				}				
				if((_time > time)) {					
					is = false;
				}
			}
			return is;
		}
	
	//需要在输入的时候非法输入和时间冲突
	public void add_Str_req(String ins_que) {
		
			//这时候的指令不是非法输入，但是需要判断时间冲突
			String []strs = ins_que.split(",|\\(|\\)");
			
			if( (isType(ins_que) == 2) && (isTimeConflict(Integer.parseInt(strs[3]))) ) {
				//内部请求				
				Request re = new Request(ins_que, true);				
				request_que.add(re);			
			}else if( (isType(ins_que) == 3) && (isTimeConflict(Integer.parseInt(strs[4]))) ){				
				//外部请求
				if( (strs[2].equals("1") && strs[3].equals("DOWN")) ||
							(strs[2].equals("10") && strs[3].equals("UP")) ) {
					System.out.println("ERROR");
					System.out.println("# format error3");
				}else {
					
						Request re = new Request(ins_que, false);
						request_que.add(re);					
				}				
			}else {
				System.out.println("ERROR");
				System.out.println("#instruction out of order!");
			}				
	}
}
