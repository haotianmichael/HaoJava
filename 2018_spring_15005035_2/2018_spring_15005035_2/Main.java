package buaaoo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Main {

	public static void main(String[] args) {
			//1.输入	
		try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))){
			String instruction = null;
			Pattern pattern;
			Pattern pattern2;
			Matcher matcher;
			Matcher matcher2;
			
			 //2.初始化命令队列
			RequestQueue requestQueue = new RequestQueue();  

			//3.判断输入并且将合法命令加入命令队列中
			do {
				instruction = br.readLine();				
				pattern = Pattern.compile("\\(FR,([1-9]|10)?,(UP|DOWN),0\\)");
				pattern2 = Pattern.compile("\\(ER,([1-9]|10)?,0\\)");
				matcher = pattern.matcher(instruction.replace(" ", ""));
				matcher2 = pattern2.matcher(instruction.replace(" ", ""));
				
				if(matcher.find() || matcher2.find()) {
					requestQueue.add_Str_req(instruction.replace(" ", ""));  
					break;
				}else if(instruction.equals("RUN")){
					break;
				}else {
					System.out.println("ERROR");
					System.out.println("#format error1");
				}				
			} while (true);
			pattern = Pattern.compile("\\(FR,([1-9]|10)?,(UP|DOWN),[0123456789]{1,}\\)");
			pattern2 = Pattern.compile("\\(ER,([1-9]|10)?,[0123456789]{1,}\\)");
			if(!instruction.equals("RUN")) {
				instruction = br.readLine();
			}
			matcher = pattern.matcher(instruction.replace(" ", ""));
			matcher2 = pattern2.matcher(instruction.replace(" ", ""));			
			while(!instruction.equals("RUN")) {  
				if(matcher.find() || matcher2.find()) {
					requestQueue.add_Str_req(instruction.replace(" ", ""));   
				}else {
					System.out.println("ERROR");
					System.out.println("#format error2");
				}
				instruction = br.readLine();
				matcher = pattern.matcher(instruction.replace(" ", ""));
				matcher2 = pattern2.matcher(instruction.replace(" ", ""));			
			}
			
			
			
			//4-1.调度
			Scheduler scheduler = new Scheduler(requestQueue.get_que());  
			scheduler._Schedule_req(); 
			
			
			
			//4-2.输出
			scheduler.Output_req();
				
				
		} catch (IOException e) {
			System.out.println("ERROR");
			System.out.println("#Invalid input!");
			System.exit(0);
		}
	}
}
