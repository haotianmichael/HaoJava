package buaaoo;

public class Request {

    private boolean type;   //指令类型
    private long time;   ///指令的发出时间
    private int floor;    //指令涉及到的楼层



    private boolean direction;  //指令的方向（楼层指令）
    private int eleNo = -1;//请求的电梯序号（内部指令）

    //构造方法
    public Request(String string, boolean is, long reqtime) {
        String []s;
        s = string.split(",|\\(|\\)");
        this.type = is;
        if(type) {
            //内部指令
            this.time = reqtime;
            char []array = s[2].toCharArray();
            this.eleNo = Integer.parseInt(String.valueOf(array[1]));
            this.floor = Integer.parseInt(s[3]);
        }else {
            //外部指令
            this.time = reqtime;
            this.floor = Integer.parseInt(s[2]);
            if(s[3].equals("UP")) {
                this.direction = true;
            }else if(s[3].equals("DOWN")){
                this.direction = false;
            }
        }
    }


    public boolean GetType() {
        return this.type;
    }

    public long GetTime() {
        return this.time;
    }

    public int GetFloor() {
        return this.floor;
    }

    public boolean GetDir() {
        return this.direction;
    }

    public int GeteleNo() {
        return this.eleNo;
    }

}
