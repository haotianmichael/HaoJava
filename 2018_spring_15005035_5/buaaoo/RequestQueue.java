package buaaoo;

import java.util.ArrayList;

public class RequestQueue {

    //命令队列
    private ArrayList<Request> req_list = new ArrayList<Request>();

    //返回的队列
    ArrayList<Request> ret = new ArrayList<Request>();

    public RequestQueue() {

    }

    //指令初始化
    public void add_req(String str_req, long req_time) {

        String[] strs = str_req.split(";");
        for(int i=0; i<strs.length; i++) {
            if(strs[i] != null) {
                String[] strss = strs[i].split(",|\\(|\\)");
                if (strss[1].equals("ER")) {
                    Request req = new Request(strs[i], true, req_time);
                    req_list.add(req);
                } else if (strss[1].equals("FR")) {
                    Request req = new Request(strs[i], false, req_time);
                    req_list.add(req);
                }
            }
        }
        if(!this.ret.isEmpty()) {
            for(int i=0; i<ret.size(); i++) {
                this.req_list.add(this.ret.get(i));
            }
        }
        sameFilter(this.req_list);
    }

    //过滤同质请求
    public ArrayList<Request> sameFilter(ArrayList<Request> sub_req) {

            Elevator sub_ele = new Elevator();
            double sub_stand = 0;   //虚拟的系统时间

            int num = sub_req.size();
            Request r = null;

            while (!sub_req.isEmpty()) {
                r = sub_req.get(0);
                sub_stand = r.GetTime();  //指令分别输出，所以起始时间是不一样的
                if (r.GetType()) {

                    if (num == sub_req.size()) {
                        if (r.GetFloor() == sub_ele.getfloor()) {
                            sub_stand += 6;
                        } else {
                            sub_stand = (r.GetFloor() - 1) * 3.0 + 6.0;
                            sub_ele.setEle(r.GetFloor(), true, false);
                        }
                    } else {
                        sub_stand = (sub_stand > r.GetTime()) ? sub_stand : r.GetTime();
                        if (r.GetFloor() == sub_ele.getfloor()) {
                            sub_stand += 6.0;
                        } else {
                            if (sub_ele.getfloor() < r.GetFloor()) {
                                sub_stand += (r.GetFloor() - sub_ele.getfloor()) * 3.0 + 6.0;
                            } else {
                                sub_stand += (sub_ele.getfloor() - r.GetFloor()) * 3.0 + 6.0;
                            }


                            if (sub_ele.getfloor() < r.GetFloor()) {
                                sub_ele.setEle(r.GetFloor(), true, false);
                            } else {
                                sub_ele.setEle(r.GetFloor(), false, false);
                            }
                        }
                    }
                    //同质请求过滤
                    ret.add(sub_req.get(0));
                    int Nomber = sub_req.get(0).GeteleNo();
                    sub_req.remove(0);

                    int i = 0;
                    while (i < sub_req.size()) {
                        double circleTime = sub_req.get(i).GetTime();
                        if (circleTime <= sub_stand) {
                            if ((sub_ele.getfloor() == sub_req.get(i).GetFloor()) && (sub_req.get(i).GetType()) &&
                                    (Nomber == sub_req.get(i).GeteleNo())) {
                                long time = System.currentTimeMillis();
                                System.out.println("#"+time+":SAME["+"ER,#"+sub_req.get(i).GeteleNo()+","+sub_req.get(i).GetFloor()+","+sub_req.get(i).GetTime()+".0]");
                                sub_req.remove(i);
                                num--;
                                i = 0;
                            } else {
                                i++;
                            }
                        } else {
                            i++;
                        }
                    }


                } else {
                    if (num == sub_req.size()) {
                        if (r.GetFloor() == sub_ele.getfloor()) {
                            sub_stand += 6;
                        } else {
                            sub_stand += (r.GetFloor() - 1) * 3.0 + 6;
                            sub_ele.setEle(r.GetFloor(), true, false);
                        }
                    } else {
                        sub_stand = (sub_stand > r.GetTime()) ? sub_stand : r.GetTime();
                        if (r.GetFloor() == sub_ele.getfloor()) {
                            sub_stand += 6;
                            if (r.GetType()) {
                                sub_ele.setDir(true);
                            } else {
                                sub_ele.setDir(false);
                            }
                        } else {
                            if (sub_ele.getfloor() < r.GetFloor()) {
                                sub_stand += (r.GetFloor() - sub_ele.getfloor()) * 3.0 + 6.0;
                                sub_ele.setEle(r.GetFloor(), true, false);
                            } else {
                                sub_stand += (sub_ele.getfloor() - r.GetFloor()) * 3.0 + 6.0;
                                sub_ele.setEle(r.GetFloor(), false, false);
                            }

                        }
                    }
                    ret.add(sub_req.get(0));
                    boolean dirction = sub_req.get(0).GetDir();
                    sub_req.remove(0);

                    int i = 0;
                    while (i < sub_req.size()) {
                        double circleTime = sub_req.get(i).GetTime();
                        if (circleTime <= sub_stand) {
                            if ((sub_ele.getfloor() == sub_req.get(i).GetFloor()) && (!sub_req.get(i).GetType())
                                    && dirction == sub_req.get(i).GetDir()) {
                               long Cutime = System.currentTimeMillis();
                               if(sub_req.get(i).GetDir()) {
                                   System.out.println("#" + Cutime + ":SAME[" + "FR," + sub_req.get(i).GetFloor() + ",UP," + sub_req.get(i).GetTime() + ".0]");
                               }else {
                                   System.out.println("#" + Cutime + ":SAME[" + "FR," + sub_req.get(i).GetFloor() + ",DOWN," + sub_req.get(i).GetTime() + ".0]");
                               }
                                sub_req.remove(i);
                                num--;
                                i = 0;
                            } else {
                                i++;
                            }
                        } else {
                            i++;
                        }
                    }
                }//else
            }//while

            return ret;

    }//function
}
