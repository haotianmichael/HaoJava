package buaaoo;

import java.util.ArrayList;

public class Elevator implements  Runnable{


    private int Curfloor = 1;  //当前楼层
    private boolean Curdirection = true; //当前方向（true为向上， false为向下）
    private boolean Curstatus = false;
    private int eleNo = -1;
    private ArrayList<Request> requestList = new ArrayList<Request>();
    private static long sub_Stime = 0;
    private int ExerciseAmount = 0;//累计运动量
    public Elevator() {}

    public Elevator(ArrayList<Request> req, int eleNo) {
        this.requestList = req;
        this.eleNo = eleNo;
    }

    public int getfloor() {
        return this.Curfloor;
    }

    public boolean getdir() {
        return this.Curdirection;
    }

    public void setEle(int flo, boolean isD, boolean isS) {
        this.Curdirection = isD;
        this.Curfloor = flo;
        this.Curstatus = isS;
    }

    public void setDir(boolean is) {
        this.Curdirection = is;
    }

    public int exercise() {
        return this.ExerciseAmount;
    }



    @Override
    public synchronized void run() {
        //在单个电梯的运行过程中只能一次运行一条指令，捎带请求应该是之前就现在调度器中判断好给哪一个电梯的
        if(requestList.isEmpty()) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        Request Mainrequest = requestList.get(0);
        requestList.remove(0);
            if (Mainrequest.GetType()) {
                if (Mainrequest.GetTime() < 0.001) {
                    if (Mainrequest.GetFloor() == this.getfloor()) {
                        try {
                            this.sub_Stime += 6.0;
                            Thread.sleep(6000);
                            long time = System.currentTimeMillis();
                            System.out.println(time+":[ER,#"+Mainrequest.GetFloor()+","+Mainrequest.GetFloor()+","+Mainrequest.GetTime()+".0]/(#"
                            +this.getfloor()+","+Mainrequest.GetFloor()+",STILL"+this.exercise()+","+this.sub_Stime+")");
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    } else {
                        for (int j = 0; j < (Mainrequest.GetFloor() - 1); j++) {
                            try {
                                Thread.sleep(6000);
                                this.sub_Stime += 6;
                                this.ExerciseAmount += 1;  //每一运行一层楼，进行累计运动量
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                        try {
                            Thread.sleep(3000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        this.setEle(Mainrequest.GetFloor(), true, false);
                        this.sub_Stime += 3;
                    }
                } else {
                    if (Mainrequest.GetFloor() == this.getfloor()) {
                        this.sub_Stime += 6.0;
                        try {
                            Thread.sleep(6000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    } else {

                    }

                }

            } else {
                //外部指令
            }
            if(requestList.isEmpty()) {
               return ;
            }
    }
}
