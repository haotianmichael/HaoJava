package buaaoo;


import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class mmain {

    public static void main(String[] args) {
        try (PrintStream ps = new PrintStream(new FileOutputStream("result.txt"))) {
           System.setOut(ps);
            int ii = 0;

            long systime = 0; //每一条指令的发出时间
            long standardtime = 0;  //第一条指令开始的时间
            Pattern pattern = Pattern.compile("(\\(FR,((([2-9]|(1[0-9])),(UP|DOWN))|(1,UP)|(20,DOWN))\\)|\\(ER,#[123],([1-9]|(1[0-9])|(20))\\))" +
                    "(;(\\(FR,((([2-9]|(1[0-9])),(UP|DOWN))|(1,UP)|(20,DOWN))\\)|\\(ER,#[123],([1-9]|(1[0-9])|(20))\\))){0,9}");


            //输入线程（主线程）
            RequestQueue requestQueue = new RequestQueue();

            //调度线程
            ALS_Scheduler als_scheduler = new ALS_Scheduler(requestQueue);
            new Thread(als_scheduler, "调度线程").start();

            while (true) {
                Scanner input = new Scanner(System.in);
                String instr = input.nextLine().replace(" ", "");
                systime = System.currentTimeMillis();
                if (ii == 0) {
                    standardtime = systime;  //系统标准时间
                }
                Matcher matcher = pattern.matcher(instr);
                if (matcher.matches()) {
                    requestQueue.add_req(instr,(systime - standardtime)/1000);
                } else if (instr.equals("END")) {
                    break;
                } else {
                    String []strings = instr.split(";");
                    for(int i=0; i<strings.length; i++) {
                        Pattern pattern1  = Pattern.compile("\\(FR,((([2-9]|(1[0-9])),(UP|DOWN))|(1,UP)|(20,DOWN))\\)|\\(ER,#[123],([1-9]|(1[0-9])|(20))\\)");
                        Matcher matcher1 = pattern1.matcher(strings[i]);
                        if(matcher1.matches()) {
                            requestQueue.add_req(strings[i], (systime - standardtime) / 1000);
                        }else{
                            //部分匹配，将错误的部分去掉
                            long time = System.currentTimeMillis();
                            System.out.println(time + ":INVALID[" + strings[i] + "," + (systime - standardtime) / 1000 + ".0]");
                        }
                    }
                                    }
                if (++ii > 50) {
                    System.out.println("#INPUT INVALID");
                    return;
                }
            }

        } catch (IOException e) {
            System.out.println("文件重定向出错！");
            return;
        }


    }

}
