package buaaoo;



public class ALS_Scheduler extends Scheduler implements  Runnable{


    private RequestQueue requestq = new RequestQueue();

    Elevator elevatorOne;
    Elevator elevatorTwo;
    Elevator elevatorThree;
    Thread t1;
    Thread t2;
    Thread t3;

    public ALS_Scheduler(RequestQueue requestQueue){
        this.requestq = requestQueue;
        elevatorOne = new Elevator(requestQueue.ret, 1);
        elevatorTwo = new Elevator(requestQueue.ret, 2);
        elevatorThree = new Elevator(requestQueue.ret, 3);
        t1 = new Thread(elevatorOne, "一号电梯线程");
        t2 = new Thread(elevatorTwo, "二号电梯线程");
        t3 = new Thread(elevatorThree, "三号电梯线程");

    }


    //同步方法：其同步监视锁为this对象
    public  synchronized void  run() {
        //电梯线程
        t1.start();
        t2.start();
        t3.start();
        while(!this.requestq.ret.isEmpty()) {
            notifyAll();
            Request Mainrequest = requestq.ret.get(0);
           if(!Mainrequest.GetType()) {


           }else  {
               if(Mainrequest.GeteleNo() == 1) {
                   try {
                       t1.join();
                   } catch (InterruptedException e) {
                       e.printStackTrace();
                   }
               }else  if(Mainrequest.GeteleNo() == 2){
                   try {
                       t2.join();
                   } catch (InterruptedException e) {
                       e.printStackTrace();
                   }
               }else if(Mainrequest.GeteleNo() == 3) {
                   try {
                       t3.join();
                   } catch (InterruptedException e) {
                       e.printStackTrace();
                   }
               }
           }
        }


    }

    public int  isPass(Request request) {
        int num = 0;

        return num;
    }

    public int isExercise(Request request) {
        int num = 0;

        return num;
    }

}