package buaaoo;


public class Monitor{

    //监控线程：实际上代表了一个锁定的监控对象，每一个监控对象都有一个触发器类和一个响应任务类来实现一些响应
    //不可变对象（线程安全类型）
    //monitor类实际上起到了一个缓冲的作用，将命令队列拆解为单个可执行的命令

    private InQueue moqueue = new InQueue();  //代表不同的监控对象
    private Trigger motrigger;   //代表一个监控线程
    private Response moresponse;   //多个线程共享资源

    public Monitor(InQueue in, Response response) {
        this.moqueue = in;
        this.moresponse = response;
    }

    public void SingleSimulateS() {

        //判断触发种类并且选择执行的模块
        if(moqueue.getIntrigger().equals("renamed")) {
            motrigger = new TrigRenamed(moqueue.getInpath(),
                    moqueue.getInresponse(),moresponse);
            new Thread(motrigger, "renamedThread").start();


        }else if(moqueue.getIntrigger().equals("modified")) {
            motrigger = new TrigModified(moqueue.getInpath(),
                    moqueue.getInresponse(), moresponse);
            new Thread(motrigger, "modifiedThread").start();


        }else if(moqueue.getIntrigger().equals("path-changed")) {
            motrigger = new TrigPathchanged(moqueue.getInpath(),
                    moqueue.getInresponse(), moresponse);
            new Thread(motrigger, "path-changedThread").start();


        }else if(moqueue.getIntrigger().equals("size-changed")) {
            motrigger = new TrigSizechanged(moqueue.getInpath(),
                    moqueue.getInresponse(), moresponse);
            new Thread(motrigger, "size-changedThread").start();

        }else {
            System.out.println("#INVALID TriggerType! Please check your input");
        }
    }
}
