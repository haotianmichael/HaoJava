package buaaoo;

public class summary {

    private int RenamedSummary = 0;
    private int ModifiedSummary = 0;
    private int SizechangedSummary = 0;
    private int PathchangedSummary = 0;

    public synchronized void addRenameSummary() {
        this.RenamedSummary++;
    }

    public synchronized  void addModifiedSummary() {
        this.ModifiedSummary++;
    }

    public synchronized void addSizechangedSummary() {
        this.SizechangedSummary++;
    }

    public synchronized void addPathchangedSummary() {
        this.SizechangedSummary++;
    }

    public int getRenamedSummary() {
        return this.RenamedSummary;
    }

    public int getModifiedSummary() {
        return this.ModifiedSummary;
    }

    public int getSizechangedSummary() {
        return this.SizechangedSummary;
    }

    public int getPathchangedSummary() {
        return this.PathchangedSummary;
    }
}
