package buaaoo;

public class TrigModified extends Trigger implements  Runnable {

    private MonitorFIle ModifiedFile;  //监控线程需要监控的文件或者文件夹
    private String ModifiedResponse;//文件响应类型
    private Response response;//响应线程指针


    public TrigModified(String path, String res, Response response) {
        ModifiedFile = new MonitorFIle(path);
        this.ModifiedResponse = res;
        this.response = response;
    }

    @Override
    public void run() {
        if (ModifiedFile.exists()) {
            if (ModifiedFile.isDirectory()) {
                //监控对象是一个文件夹
                System.out.println("这是第一个文件夹！");
            } else if (ModifiedFile.isFile()) {
                //监控对象为一个文件
                long time = ModifiedFile.getLastModifytime();
                long delayTime = System.currentTimeMillis();

                while(true) {
                    for(int i=0; i<2; i++) {
                        Thread.yield();  //暂停几秒，但是可以直接进入到CPU状态
                    }
                   // System.out.println("循环");
                    //考虑到文件不在的情况  该触发失效
                    if(!ModifiedFile.exists()){
                        long currentDelayTime = System.currentTimeMillis();
                        if((currentDelayTime - delayTime)/1000 >= 100) {
                            System.out.println("#被监控文件不存在   #线程结束");
                            break;
                        }
                    }else {
                        long curtime = ModifiedFile.getLastModifytime();
                        if (curtime != time) {
                            //触发条件
                            if (this.ModifiedResponse.equals("record-summary")) {
                                response.recoredSummary("modified");
                                System.out.println("触发成功");

                            } else if (this.ModifiedResponse.equals("record-detail")) {
                                response.recordModifiedDetail(time, curtime, ModifiedFile.getName());
                                System.out.println("触发成功");

                            } else if (this.ModifiedResponse.equals("recover")) {
                                System.out.println("#操作错误！  Can't resolve symbol recover on modified operation!  #线程终止");
                                break;
                            }
                            time = curtime;//以便下一次继续监控
                        } else {
                            long currentDelayTime = System.currentTimeMillis();
                            if ((currentDelayTime - delayTime) / 1000 >= 50) {
                                System.out.println("#在规定的50s之内没有触发或一次触发结束   #线程" + Thread.currentThread().getName() + "结束");
                                break;
                            }
                        }
                    }
                }//while
            }
        }else {
            System.out.println("###########################被监控文件不存在   #监控线程终止，请自行终止响应程序！");
        }
    }//run



}
