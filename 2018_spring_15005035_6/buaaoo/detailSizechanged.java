package buaaoo;


public class detailSizechanged extends detail {
    private long beforelength;
    private long afterlength;
    private long beforetime;
    private long aftertime;
    private String namefile;

    public detailSizechanged(long beforeT, long afterT,
                             long beforeL, long afterL,
                             String name) {
        this.afterlength = afterL;
        this.beforelength = beforeL;
        this.aftertime = afterT;
        this.beforetime = beforeT;
        this.namefile = name;
    }

    public String getNamefile() {
        return this.namefile;
    }

    public long getBeforelength() {
        return this.beforelength;
    }

    public long getAfterlength() {
        return this.afterlength;
    }


    public long getBeforetime() {
        return this.beforetime;
    }


    public long getAftertime() {
        return this.aftertime;
    }


}
