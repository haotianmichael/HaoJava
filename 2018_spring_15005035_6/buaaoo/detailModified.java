package buaaoo;


public class detailModified extends detail {

    private String filename;
    private long beforetime;
    private long aftertime;

    public detailModified(long bdfore, long after, String name ) {
        this.aftertime = after;
        this.beforetime = bdfore;
        this.filename = name;
    }

    public String getFilename(){
        return this.filename;
    }

    public long getBeforetime() {
        return this.beforetime;
    }

    public long getAftertime() {
        return this.aftertime;
    }
}
