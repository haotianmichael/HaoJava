package buaaoo;

public class Test implements  Runnable{

    @Override
    public void run() {
        /**
         *测试线程：
         *      本程序所有的文件操作都在MonitorFile类（线程安全）中，若测试者想手动文件测试，可通过shell脚本直接在命令行
         *  修改文件，或者可以通过修改本线程的部分代码，实现测试功能
         *  注1：一次测试线程只能进行一次文件修改操作
         *  注2：如果测试线程不起作用，请手动使用shell进行文件修改操作，以此带来的不便请见谅（重点是监控）
         *  注3：本监控程序是针对文件操作，所以在过程中有涉及到文件夹的创建请测试者自行解决（比如path-changed中的子目录），带来不便请见谅
         * */
        System.out.println("test By hao-tian-micnael start");

        MonitorFIle monitorFIle = new MonitorFIle("./vince/a.txt");
        if(!monitorFIle.exists()) {
            monitorFIle.creatNewFile("./vince/a.txt");
        }



        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        monitorFIle.renameTo("./vince/bb.txt");  //renamed


        //monitorFIle.setLastMocifiedTime(System.currentTimeMillis());  //modified


        //monitorFIle.delete();  //object-deleted over


        //monitorFIle.setlength(monitorFIle.getPath(), true);  //size-changed


        System.out.println("test terminated By haotian-michael ");


        //monitorFIle.setPath("./vince/aaa/b.txt");
    }
}
