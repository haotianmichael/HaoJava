package buaaoo;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class mmain {

    public static void main(String[] args) {

        ArrayList<InQueue> queue = new ArrayList<InQueue>();
        boolean issame = false;
        try {
            BufferedReader std = new BufferedReader(new InputStreamReader(System.in));
            String string = new String();
            while (true) {
                string = std.readLine();
                String [] strs = string.split(" ");
                if(strs[0].equals("IF") && strs[3].equals("THEN") && strs.length == 5) {
                    InQueue in = new InQueue(strs[1], strs[2], strs[4]);

                    //判断相同的命令
                    for(int i=0; i<queue.size(); i++){
                        if(queue.get(i).equals(in)) {
                            issame = true;
                            System.out.println("#sameMonitoring");
                            break;
                        }
                    }

                    if(!issame) {
                        queue.add(in);
                    }
                }else if(string.equals("END")) {
                    break;
                }else {
                    System.out.println("#INVALID INPUT");
                }
            }

            //输入结束，监控平台开始工作
            MonitorSimulate monitorSimulate = new MonitorSimulate(queue);
            monitorSimulate.simulateS();


        }catch (IOException e) {
            System.out.println("Unexpected Crash！");
        }
    }
}
