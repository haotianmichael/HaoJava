package buaaoo;
import java.util.ArrayList;

public class MonitorSimulate {

    private ArrayList<InQueue> inQueues = new ArrayList<InQueue>();  //监控线程队列
    private Response response = new Response();
    private Test test = new Test();

    public MonitorSimulate(ArrayList<InQueue> queues) {
        this.inQueues = queues;
    }



    public void simulateS() {
        try {

            //监控线程
            for(int i=0; i<inQueues.size(); i++) {
                new Monitor(inQueues.get(i), response).SingleSimulateS();
            }

            //响应线程(后台线程 = false）
            Thread respondThread = new Thread(response, "响应线程");
            //respondThread.setDaemon(true);
            respondThread.start();

            //测试线程程序
            Thread.sleep(5000);
            new Thread(test, "测试线程").start();

        }catch (Exception ex) {
            System.out.println(ex);
        }
    }
    
}
