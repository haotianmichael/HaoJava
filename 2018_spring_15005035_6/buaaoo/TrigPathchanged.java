package buaaoo;

import java.io.File;
import java.util.Arrays;

public class TrigPathchanged extends Trigger implements Runnable{

    private MonitorFIle PathchangedFile;
    private String Pathchangedresponse;
    private Response response;

    public TrigPathchanged(String path, String res, Response response) {
        this.PathchangedFile = new MonitorFIle(path);
        this.Pathchangedresponse = res;
        this.response = response;
    }


    @Override
    public void run() {
        if(PathchangedFile.exists()) {
           if(PathchangedFile.isDirectory()){
               //监控对象是整个文件夹
               System.out.println("文件夹存在");
           }else if(PathchangedFile.isFile()){
               //监控对象是整个文件
               long time = PathchangedFile.getLastModifytime();
               long length = PathchangedFile.getLength();
               String path = PathchangedFile.getPath();
               String beforename = PathchangedFile.getName();
               int index = PathchangedFile.getPath().lastIndexOf("/");
               String direPath = PathchangedFile.getPath().substring(0,index);
               MonitorFIle direPathFile = new MonitorFIle(direPath); //根目录下路径
               long delayTime = System.currentTimeMillis();

               File [] listDire = new File[10];  //最多十个监控文件
               int k=0;
               for(File filelists : direPathFile.listFiles()) {
                   if(filelists.isDirectory()) {
                       listDire[k++] = filelists;  //获得根目录下的所有文件夹
                   }
               }
               while(true) {
                   for(int i=0; i<2; i++) {
                       Thread.yield();
                   }
                   if(!PathchangedFile.exists()) {

                       boolean is = false;
                       boolean isis = false;
                       //在记录的子目录中寻找
                       for(int j = 0; j<listDire.length && (listDire[j]!=null); j++) {
                           File [] list = listDire[j].listFiles();
                           for(File file : list) {
                               if(file.getName().equals(beforename) &&
                                       (file.lastModified()==time) &&
                                       (file.length() == length)) {
                                   //触发条件
                                   is = true;
                                   if(this.Pathchangedresponse.equals("record-summary")) {
                                       response.recoredSummary("path-changed");
                                       System.out.println("触发成功");

                                   }else if(this.Pathchangedresponse.equals("record-detail")) {
                                       response.recordPathchangedDetail(path, file.getPath(), PathchangedFile.getName());
                                       System.out.println("触发成功");

                                   }else if(this.Pathchangedresponse.equals("recover")) {
                                       System.out.println("触发成功");

                                       //恢复原有路径
                                       File file4 = new File(path);
                                       boolean move = file.renameTo(file4);
                                       if (move){
                                           System.out.println("路径转移成功！");
                                       }else{
                                           System.out.println("路径转移失败！  #请重新操作！");
                                       }
                                   }
                                   isis = true;
                                   break;
                               }
                           }//for_1
                           if(is) {
                               break;
                           }else {
                               long currentDelayTime = System.currentTimeMillis();
                               if((currentDelayTime - delayTime) /  1000 >= 10) {
                                   System.out.println("#监控失败！ 被监控对象在规定10s时间内已被删除  线程终止");
                                   isis = true;
                                   break;
                               }
                           }
                       }//for_2
                       if(isis) {
                           break;
                       }
                   }else {
                       long currentDelayTime = System.currentTimeMillis();
                       if((currentDelayTime - delayTime) /  1000 >= 50) {
                           System.out.println("#在规定的50s之内没有触发或一次触发结束   #线程结束" + Thread.currentThread().getName() + "终止");
                           break;
                       }
                   }
               }//while
           } //isFile
        }else {
            System.out.println("###########################被监控文件不存在   #监控线程终止，请自行终止响应程序！");
        }
    }//run

}
