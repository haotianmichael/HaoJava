package buaaoo;


public class detailPathchanged extends detail {
    private String beforepath;
    private String afterpath;
    private String namefile;


    public detailPathchanged(String before, String after, String name) {
        this.afterpath = after;
        this.beforepath = before;
        this.namefile = name;
    }

    public String getNamefile() {
        return this.namefile;
    }

    public String getBeforepath() {
        return this.beforepath;
    }

    public String getAfterpath() {
        return this.afterpath;
    }
}
