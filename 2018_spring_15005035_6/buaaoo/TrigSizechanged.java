package buaaoo;

public class TrigSizechanged extends Trigger implements Runnable{

    private MonitorFIle SizechangedFile;  //监控线程需要监控的文件/文件夹
    private String Sizechangedresponse;  //文件响应类型
    private Response response;   //响应线程指针


    public TrigSizechanged(String path, String res, Response response) {
        this.SizechangedFile = new MonitorFIle(path);
        this.Sizechangedresponse = res;
        this.response = response;
    }

    @Override
    public void run() {
        if(SizechangedFile.exists()) {
            if(SizechangedFile.isDirectory()) {
                //被监控对象是一个文件
                System.out.println("文件夹存在");
            }else if(SizechangedFile.isFile()) {
                //被监控对象是单个文件
                long time = SizechangedFile.getLastModifytime();
                long length = SizechangedFile.getLength();
                long delay = System.currentTimeMillis();
                while(true) {

                    for(int i=0; i<2; i++) {
                        Thread.yield();//暂停几秒总，但是可以直接进入CPU状态
                    }
                    //考虑到文件不在的情况，该触发失效
                    if(!SizechangedFile.exists()) {
                        long currentDelayTime = System.currentTimeMillis();
                        if((currentDelayTime - delay) / 1000 >= 100) {
                            System.out.println("#被监控不存在!   #线程结束");
                            break;
                        }
                    }else {
                        long curtime = SizechangedFile.getLastModifytime();
                        long curlength = SizechangedFile.getLength();
                        if((curlength != length) && (curtime != time)) {
                            //触发条件
                            if(this.Sizechangedresponse.equals("record-summary")) {
                                response.recoredSummary("size-changed");
                                System.out.println("触发成功");

                            }else if(this.Sizechangedresponse.equals("record-detail")) {
                                response.recordSizechangedDetail(time, curtime, length, curlength, SizechangedFile.getName());
                                System.out.println("触发成功");

                            }else if(this.Sizechangedresponse.equals("recover")) {
                                System.out.println("#操作错误！  Can't resolve symbol recover on size-changed operation!  #线程终止");
                                break;
                            }
                            time = curtime;
                            length = curlength; //以便下一次监视
                        }else  {
                            long currenDelayTime = System.currentTimeMillis();
                            if((currenDelayTime - delay) / 1000 >= 50) {
                                System.out.println("#在规定的50s之内没有触发或一次触发结束   #线程"+ Thread.currentThread().getName()+ "结束");
                                break;
                            }
                        }//结束
                    }

            }//while
        }
        }else {
          System.out.println("###########################被监控文件不存在   #监控线程终止，请自行终止响应程序！");
        }
    }//run

}
