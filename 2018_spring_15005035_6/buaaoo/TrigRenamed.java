package buaaoo;
import java.io.File;

public class TrigRenamed extends Trigger implements  Runnable{

    private MonitorFIle RenameFile;  //监控线程需要监控的文件/文件夹
    private String Renameresponse;//文件响应类型
    private Response response; //响应线程指针

    public TrigRenamed(String path, String  res,Response response) {
        RenameFile = new MonitorFIle(path);
        this.Renameresponse = res;
        this.response = response;
    }

    @Override
    public void run() {
        if(RenameFile.exists()) {
            if (RenameFile.isDirectory()) {
                //监控对象为整个目录
                System.out.println("文件夹存在！");
            } else if (RenameFile.isFile()) {
                //监控对象为单个文件
                long time = RenameFile.getLastModifytime();
                long length = RenameFile.getLength();
                int index = RenameFile.getPath().lastIndexOf("/");;
                String direPath = RenameFile.getPath().substring(0, index);
                String beforename = RenameFile.getName();
                long delayTime = System.currentTimeMillis();

                while (true) {
                    for(int i=0; i<2; i++) {
                        Thread.yield();//暂停几秒，但是可以直接进入到CPU调度状态
                    }
                    if (!RenameFile.exists()) {
                        //在当期目录下寻找相同的lastmodifyTime文件
                        File filedire = new File(direPath);
                        File[] filename = filedire.listFiles(); //获得所有当前目录下的文件
                        boolean is = false;
                        for (File name : filename) {
                            if (name.lastModified() == time && name.length() == length) {
                                RenameFile = new MonitorFIle(name.getPath());//导致recover之后可以继续被触发
                                //触发rename条件.判断响应任务类型
                                if (this.Renameresponse.equals("record-summary")) {
                                    response.recoredSummary("renamed");
                                    System.out.println("触发成功");

                                } else if (this.Renameresponse.equals("record-detail")) {
                                    response.recoredRenamedDetail(beforename, name.getName());
                                    System.out.println("触发成功");

                                } else if (this.Renameresponse.equals("recover")) {
                                    //将重命名的文件恢复原状（原来的名字）
                                    System.out.println("触发成功");
                                    File newFile = new File(direPath + "/" + beforename);
                                    boolean flag = name.renameTo(newFile);
                                    if (flag) {
                                        System.out.println("文件名称恢复成功！");
                                    } else {
                                        System.out.println("文件名称恢复失败！");
                                    }
                                }
                                is = true;
                                break;
                            }
                        }//for
                        if(!is) {
                            //在整个当前的目录下监控文件被删除
                            long currentDelayTime = System.currentTimeMillis();
                            if((currentDelayTime - delayTime) / 1000 >= 10) {
                                System.out.println("#监控失败！监控对象在10秒之内已被删除！   #线程" + Thread.currentThread().getName() + "终止");
                                break;
                            }

                        }
                    } else {
                        long currenrtDelayTime = System.currentTimeMillis();
                        if ((currenrtDelayTime - delayTime)/1000 >= 50) {
                            System.out.println("#在规定的50s之内没有触发或一次触发结束   #线程" + Thread.currentThread().getName() + "终止");
                            break;
                        }
                    }
                }//while
            }
        }else {
            System.out.println("###########################被监控文件不存在   #监控线程终止，请自行终止响应程序！");
        }
    }//run
}