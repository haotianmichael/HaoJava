package buaaoo;

public class InQueue {

    private String inpath = null;
    private String intrigger = null;
    private String inresponse = null;

    public InQueue(String path, String trigger, String response) {
        this.inpath = path;
        this.intrigger = trigger;
        this.inresponse = response;
    }

    public InQueue() {

    }

    public String getInpath() {
        return this.inpath;
    }

    public String getIntrigger() {
        return this.intrigger;
    }

    public String getInresponse() {
        return this.inresponse;
    }

    public void start() {

    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof InQueue) {
            if (((InQueue) obj).inpath.equals(this.inpath)
                    && ((InQueue) obj).inresponse.equals(this.inresponse)
                    && ((InQueue) obj).intrigger.equals(this.intrigger)) {
                return true;
            }else {
                return false;
            }
        }else {
            return false;
        }
    }




}
