package buaaoo;


import java.io.*;
import java.util.ArrayList;

public class Response implements Runnable{

    private summary s = new summary();
    private ArrayList<detail> details = new ArrayList<detail>();


    public Response() {

    }

    public void recoredSummary(String responseType) {

            if (responseType.equals("renamed")) {
                s.addRenameSummary();
            } else if (responseType.equals("size-changed")) {
                s.addSizechangedSummary();
            } else if (responseType.equals("path-changed")) {
                s.addPathchangedSummary();
            } else if (responseType.equals("modified")) {
                s.addModifiedSummary();
            }

    }



    //需要记录详细信息
    public synchronized void recoredRenamedDetail(String before, String after) {
        detail d = new detailRenamed(before, after);
        details.add(d);
    }

    public synchronized void recordModifiedDetail(long before, long after, String name) {
        detail d = new detailModified(before, after, name);
        details.add(d);
    }

    public synchronized void recordSizechangedDetail(long beforeT, long afterT,
                                                     long beforeL, long afterL,
                                                     String name) {
        detail d = new detailSizechanged(beforeT, afterT, beforeL, afterL, name);
        details.add(d);
    }

    public synchronized void recordPathchangedDetail(String before, String after, String name) {
        detail d = new detailPathchanged(before, after, name);
        details.add(d);
    }


    @Override
    public  void run() {
        File file = new File("./response.txt");
        if(!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            PrintStream ps = new PrintStream(new FileOutputStream("./response.txt"));
            //System.setOut(ps);将输出重定向
        while(true) {
            System.out.println("迄今为止的所有触发成功的detail响应（每20s刷新一次）：");
            int len = details.size();
            if(len == 0) System.out.println("None");
            for(int i=0; i<len; i++) {
                if(details.get(i) instanceof  detailModified) {
                    System.out.print("Type：modified\t");
                    System.out.print("filename:" + ((detailModified) details.get(i)).getFilename()+ "\t");
                    System.out.print("the time before modified:" + ((detailModified) details.get(i)).getBeforetime()+"\t");
                    System.out.print("the time after modified:" + ((detailModified) details.get(i)).getAftertime()+"\t");

                }else if(details.get(i) instanceof  detailPathchanged) {
                    System.out.print("Type: path-changed\t");
                    System.out.print("filename:"+ ((detailPathchanged) details.get(i)).getNamefile()+"\t");
                    System.out.print("the path before changed:" + ((detailPathchanged) details.get(i)).getBeforepath()+"\t");
                    System.out.print("the path after changed:" + ((detailPathchanged) details.get(i)).getAfterpath()+ "\t");

                }else if(details.get(i) instanceof detailRenamed) {
                    System.out.print("Type: renamed\t");
                    System.out.print("the name before renamed:" + ((detailRenamed) details.get(i)).getNamebeforechanged()+"\t");
                    System.out.print("the name after renamed:" + ((detailRenamed) details.get(i)).getNameafterchanged()+"\t");

                }else if(details.get(i) instanceof  detailSizechanged){
                    System.out.print("Type: size-changed\t");
                    System.out.print("the time before changed:" + ((detailSizechanged) details.get(i)).getBeforetime() + "\t");
                    System.out.print("the time after changed:" + ((detailSizechanged) details.get(i)).getAftertime()+ "\t");
                    System.out.print("the size before changed:" + ((detailSizechanged) details.get(i)).getBeforelength()+ "\t");
                    System.out.print("the size after changed:" + ((detailSizechanged) details.get(i)).getAfterlength() + "\t");

                }else {
                    System.out.println("刷新失败！  请重新操作！");
                }
               System.out.println();
            }


            System.out.println("-------------------------------");
            System.out.println("迄今为止的所有触发成功的summary响应（每20s刷新一次）：");
            {
                System.out.println("summary By renamed:\t" + s.getRenamedSummary()+"  times");
                System.out.println("summary By path-changed:\t" + s.getPathchangedSummary()+"  times");
                System.out.println("summary By path-changed:\t" + s.getPathchangedSummary()+"  times");
                System.out.println("summary By size-changed:\t" + s.getSizechangedSummary()+ "  times");
                System.out.println("summary By modified:\t" + s.getModifiedSummary() + "times");
            }
            System.out.println("一次刷新结束， 请等待！");
            System.out.println();
            System.out.println();
            System.out.println();


            try {
                Thread.sleep(20000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println();
            System.out.println();
            System.out.println();

        }//while
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
