package buaaoo;

import java.io.*;
import java.util.ArrayList;

public class OutputSys {

   
    public int StartX;
    public int StartY;
    public int EndX;
    public int EndY;
    public long time;
    public ArrayList<Taxi> taxiRequired;

    
    public int ID;
    public int Tx;
    public int Ty;

    
    public long getOrderTime;
    public long passengerTime;
    public long arrivalTIme;
    
    public OutputSys(int Sx, int Sy, int Ex, int Ey, long time, ArrayList<Taxi> taxiRequired) {
        this.time = time;
        this.EndY = Ey;
        this.EndX  = Ex;
        this.StartY = Sy;
        this.StartX = Sx;
        this.taxiRequired = (ArrayList<Taxi>) taxiRequired.clone();
    }
    
    public OutputSys(int Sx, int Sy, int Ex, int Ey, long time) {
        this.StartX = Sx;
        this.StartY = Sy;
        this.EndX = Ex;
        this.EndY = Ey;
        this.time = time;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof OutputSys) {
            if(this.EndY == ((OutputSys) obj).EndY &&
                    this.EndX == ((OutputSys) obj).EndX &&
                    this.StartY == ((OutputSys) obj).StartY &&
                    this.StartX == ((OutputSys) obj).StartX) {

                return true;
            }else {
                return  false;
            }
        }else {
            return false;
        }
    }

    public void getTaxi(int ID, int x, int y, long time) {
        this.ID = ID;
        this.Tx = x;
        this.Ty = y;
        this.getOrderTime = time;
    }

    public void getArrail(long time ) {
        this.arrivalTIme = time;
    }

    public void getPassengerTime(long time) {
        this.passengerTime = time;
    }

    public long handlerTime(long time ) {
        return time/100*100;
    }

    public void print() {

        try {
            PrintStream ps = new PrintStream(new FileOutputStream("./#"+this.ID+"Taxi.txt"));
            System.setOut(ps);
            System.out.println("这是 "+this.ID + " 号出租车("+this.Ty+", "+this.Tx+")  在时刻："
                    + this.handlerTime(this.time) + "接到的订单" );
            System.out.println("起始位置： ("+this.StartY+"， " + this.StartX+")" );
            System.out.println("目标位置：（" + this.EndY+", "+this.EndX+")");
            System.out.println();
            System.out.println();


            System.out.println("----------------------抢单的出租车细节信息");
            System.out.println("车辆编号\t车辆当前位置\t车辆当前状态\t车辆信用");
            for(int i=0; i<taxiRequired.size(); i++) {
                System.out.print(taxiRequired.get(i).getID()+"\t");
                System.out.print("(" + taxiRequired.get(i).getY() + ", " +taxiRequired.get(i).getX() + ")\t");
                System.out.print(taxiRequired.get(i).getState()+"\t");
                System.out.println(taxiRequired.get(i).getCredit()+"\t");
            }
            System.out.println("----------------------抢单的出租车细节信息");
            System.out.println();
            System.out.println();


            System.out.println("-----------------------派单出租车运行细节信息");
            System.out.println("接单时间为："+this.handlerTime(this.getOrderTime)+"此时出租车的位置为：(" + this.Ty + ", " + this.Tx+ ")");
            System.out.println("出租车到达乘客指定地点的时间为" + this.handlerTime(this.passengerTime));
            System.out.println("出租车到达目标时间为：" + this.handlerTime(this.arrivalTIme));

            System.out.println("-----------------------下面分别为为出租车途径分支点的坐标和时刻");
            

        } catch (IOException e) {
            System.out.println("文件输出错误");
        }
    }
    
}
