package buaaoo;

import java.awt.*;
import java.util.ArrayList;
import java.util.Random;

public class Taxi implements  Runnable{

    private int ID;
    private int[][]taxiMap;
    private int[][]taxiMatrix;


    private int x = -1;
    private int y = -1;
    private state TaxiState;
    private int credit = 0;

    public enum state {
        still, approaching, service, waiting;
    }

    private OutputSys taxiReqest;

    TaxiGUI taxiGUI = new TaxiGUI();
    Point point = new Point();

    public Taxi(int map[][], int matrix[][], int ID){
        this.taxiMap = map;
        this.taxiMatrix = matrix;
        Random random = new Random();
        x = random.nextInt(80);
        y = random.nextInt(80);
        credit = 0;
        this.ID = ID;
        this.TaxiState = state.waiting;

         point.move(this.x, this.y);
        taxiGUI.SetTaxiStatus(this.ID, point, 2);
    }

    public void UpdateReq(OutputSys TaxiRequest) {
        this.taxiReqest  = TaxiRequest;
        this.taxiReqest.getTaxi(this.ID, this.x,  this.y, System.currentTimeMillis());
    }


    public synchronized void setTaxiState(String name) {
        for(state e : state.values()) {
            if(e.toString().equals(name)) {
                this.TaxiState = e;
            }
        }
    }


    public synchronized String getState() {
        return this.TaxiState.toString();
    }


    public int getX() {
        return this.x;
    }
    public int getY() {
        return this.y;
    }


    public synchronized int getCredit() {
        return this.credit;
    }

    public int getID() {
        return this.ID;
    }

    public synchronized void setCredit(String Type) {
        if(Type.equals("passengers")) {
            this.credit = this.credit + 3;
        }else if(Type.equals("OutputSys")){
            this.credit++;
        }
    }

    
    /*********出租车运动函数相关******/

    public void goEast() {
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        this.x++;
    }

    public void goWest() {
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        this.x--;
    }

    public void goSouth() {
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        this.y++;
    }

    public void goNorth() {
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        this.y--;
    }
    
    public void chooseDirection() {
        ArrayList<String> direction = new ArrayList<String>();


        if((this.taxiMap[this.x][this.y] == 1) || (this.taxiMap[this.x][this.y] == 3)) {
            direction.add("south");
        }else if((this.taxiMap[this.x][this.y] == 2) || (this.taxiMap[this.x][this.y] == 3)) {
            direction.add("east");
        }

        if(this.y != 0) {
            if ((this.taxiMap[this.x][this.y - 1] == 1) || (this.taxiMap[this.x][this.y - 1] == 3)) {
                direction.add("north");
            }
        }

        if(this.x != 0) {
            if ((this.taxiMap[this.x - 1][this.y] == 2) || (this.taxiMap[this.x - 1][this.y] == 3)) {
                direction.add("west");
            }
        }


        //开始随机分配
        Random random = new Random();
        int number = random.nextInt(direction.size());


        //确定行驶方向
        String confirmDir = direction.get(number);
        if(confirmDir.equals("east")) {
            this.goEast();

            point.move(this.x, this.y);
           taxiGUI.SetTaxiStatus(this.ID, point, 2);
        }else if(confirmDir.equals("west")) {
            this.goWest();

           point.move(this.x, this.y);
           taxiGUI.SetTaxiStatus(this.ID, point, 2);
        }else if(confirmDir.equals("south")) {
            this.goSouth();

           point.move(this.x, this.y);
           taxiGUI.SetTaxiStatus(this.ID, point, 2);
        }else if(confirmDir.equals("north")) {
            this.goNorth();
            point.move(this.x, this.y);
            taxiGUI.SetTaxiStatus(this.ID, point, 2);

        }


    }
    
    /***********出租车运动函数相关********/



    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Taxi) {
            if(this.ID == ((Taxi) obj).ID) {
                return true;
            }else {
                return false;
            }
        }else {
            return false;
        }
    }

    @Override
    public  void run() {

        long CurrentTime = System.currentTimeMillis();
        while(true) {
            if((System.currentTimeMillis() - CurrentTime) >= 20000){
                this.setTaxiState("still");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                this.setTaxiState("waiting");
                CurrentTime = System.currentTimeMillis();
            }else{
                //waiting状态随机
                {
                    this.chooseDirection();
                }
                //进入接单状态
                switch (this.TaxiState) {
                    case approaching:
                        synchronized (this) {           //接单开始，出租车前往乘客出发点
                            int PassengerX = this.taxiReqest.StartX;
                            int PassengerY = this.taxiReqest.StartY;
                            int AimX  = this.taxiReqest.EndX;
                            int AimY  = this.taxiReqest.EndY;

                            System.out.println("第" + this.ID + "号出租车成功接到订单！恭喜" );
                            guiInfo gui = new guiInfo();
                            point.move(this.x, this.y);
                            taxiGUI.SetTaxiStatus(this.ID, point, 3);

                            int disPassenger = gui.distance(this.x, this.y, this.taxiReqest.StartX, this.taxiReqest.StartY, this);
                            try {
                                Thread.sleep(200*disPassenger);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            this.taxiReqest.getPassengerTime(System.currentTimeMillis());  //返回到达乘客的时间

                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            this.setTaxiState("service"); //服务开始


                           int disArrail =  gui.distance(this.x, this.y, this.taxiReqest.EndX, this.taxiReqest.EndY, this);
                            try {
                                Thread.sleep(200*disArrail);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            this.taxiReqest.getArrail(System.currentTimeMillis());  //返回到达目的地的时间
                            

                            System.out.println("第" + this.ID +"号出租车成功完成订单！ 恭喜");
                            this.taxiReqest.print();
                            point.move(this.x, this.y);
                            taxiGUI.SetTaxiStatus(this.ID, point, 1);

                            this.setCredit("passenger");
                            this.setTaxiState("still");  //一秒停滞状态
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            this.setTaxiState("waiting");  //恢复服务状态
                            CurrentTime = System.currentTimeMillis();
                        }
                }//switch


            }//else
        }//while
    }//run


}
