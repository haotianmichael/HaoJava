package buaaoo;

public class SearchingByStatues implements Runnable {


    private String status = null;
    private Taxi[] taxis;

    public SearchingByStatues(String status, Taxi[] taxiList) {
        this.status = status;
        this.taxis = taxiList;

    }

    @Override
    public void run() {
        if (status.equals("waiting") || status.equals("approaching") ||
                status.equals("still") || status.equals("service")) {

            System.out.println("这是在时刻：" + System.currentTimeMillis() + "系统查询到的在" + this.status + "状态的出租车编号：");
            for (int i = 0; i < 100; i++) {
                if (this.taxis[i].getState().equals(this.status)) {
                    System.out.println(this.taxis[i].getID() + "号出租车");
                }
            }
            System.out.println("查询结束");
        } else {
            System.out.println("查询状态输入有误！");
        }
    }

}

