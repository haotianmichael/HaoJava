package buaaoo;

import java.util.*;

public class DispatchThread implements  Runnable{

    private static int PRE_TIME = 3000;
    private int [][]map;
    private Taxi[] taxiList;
    private ArrayList<Taxi> taxiRequired = new ArrayList<Taxi>();  //满足调度的出租车（不需要线程安全）

    private int StartX;
    private int StartY;
    private int EndX;
    private int EndY;
    private long time;

    public DispatchThread(Taxi[] taxiList, int StartX, int StartY, int EndX, int EndY, long time, int map[][]) {
        this.taxiList = taxiList;  //每一个调度线程都要有个地图
        this.StartX = StartX;
        this.StartY = StartY;
        this.EndX = EndX;
        this.EndY = EndY;
        this.time = time;
        this.map = map;
    }

    /**
     * 系统调度函数
     */
    public void Simulator() {

        long StartTime = System.currentTimeMillis();
        
        System.out.println("---抢单中---");
        while(((System.currentTimeMillis() - StartTime))<PRE_TIME && (this.FliterTaxi())){}
        System.out.println("----抢单结束---");

        this.ConfirmTaxi();
    }

    public boolean FliterTaxi() {
        int XLeft = StartX - 2;
        int Xright = (StartX + 2) >= 79 ? 79 : (StartX + 2);
        int YLeft = StartY - 2;
        int Yright = (StartY + 2) >= 79 ? 79 : (StartY + 2);
        for(int i=0; i<100; i++) {
            if(taxiList[i].getState().equals("waiting") && (taxiList[i].getX()<=Xright && taxiList[i].getX()>=XLeft)
                    &&(taxiList[i].getY()<=Yright && taxiList[i].getY()>=YLeft)) {
                boolean is = false;
                for(int k = 0; k<taxiRequired.size(); k++) {
                    if(taxiRequired.get(k).equals(taxiList[i])) {
                        is = true;
                        break;
                    }
                }
                if(!is) {
                    taxiList[i].setCredit("OutputSys"); //抢单成功加分一
                    taxiRequired.add(taxiList[i]);
                    System.out.println(taxiList[i].getID()+ "号出租车抢单成功！ 等待系统调度中");
                }
            }
        }
        return true;
    }

    public void ConfirmTaxi() {
        Comparator<Taxi> OrderCredit = new Comparator<Taxi>() {

            @Override
            public int compare(Taxi o1, Taxi o2) {
                return  (o2.getCredit() - o1.getCredit());
            }
        };
        Queue<Taxi> filterTaxiFir = new PriorityQueue<>(OrderCredit);

        if(taxiRequired.isEmpty()) {
            System.out.println("#No Taxi available");
        }else {

            for(int j=0; j<taxiRequired.size(); j++) {
               filterTaxiFir.add(taxiRequired.get(j));
            }
            OutputSys RequestConfired  = new OutputSys(StartX, StartY, EndX, EndY, time, this.taxiRequired);  //产生新的订单

            if(filterTaxiFir.size() == 1) {
                System.out.println("只有"+filterTaxiFir.element().getID()+"号出租车接到订单");
                filterTaxiFir.element().UpdateReq(RequestConfired);  //更新订单
                filterTaxiFir.element().setTaxiState("approaching");  //更新出租车状态

            }else {
                System.out.println("有多辆出租车， 分别是：");
                for(int i=0; i<taxiRequired.size(); i++) {
                    System.out.println(taxiRequired.get(i).getID()+"号出租车");
                }
                System.out.println("开始调度出租车队列");
                ArrayList<Taxi> filter = new ArrayList<Taxi>();
                //单元最短路径
                filter.add(filterTaxiFir.peek());
                filterTaxiFir.poll();
                while(!filterTaxiFir.isEmpty() && filter.get(0).getCredit() == filterTaxiFir.peek().getCredit()){
                    filter.add(filterTaxiFir.peek());
                    filterTaxiFir.poll();
                }

                int distance = bfs(filter.get(0).getX(), filter.get(0).getY(), StartX, StartY, filter.get(0));
                int I = 0;
                for(int i=1; i<filter.size(); i++) {
                    if(bfs(filter.get(i).getX(), filter.get(i).getY(), StartX, StartY, filter.get(i)) <= distance && filter.get(i).getState().equals("waiting")) {
                        I = i;
                        distance = bfs(filter.get(i).getX(), filter.get(i).getY(), StartX, StartY, filter.get(i));
                    }
                }
                System.out.println(filter.get(I).getID()+" 号出租车调度成功！");
                filter.get(I).UpdateReq(RequestConfired);
                filter.get(I).setTaxiState("approaching");

            }
        }//else
    }

    public int bfs(int x1, int y1, int x2, int y2, Taxi Runtaxi) {
        guiInfo gui = new guiInfo();
        return gui.distance(x1, y1, x2, y2, Runtaxi);
    }


    @Override
    public void run() {
        this.Simulator();
    }
}
