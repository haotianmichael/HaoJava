package buaaoo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class InputSys {
    
    private static Pattern PATTERN_NUMBER = Pattern.compile("\\[CR,\\(([0-9]|([1-7][0-9])),([0-9]|([1-7][0-9]))\\),\\(([0-9]|([1-7][0-9])),([0-9]|([1-7][0-9]))\\)\\]");

    private int[][] map;
    private int[][] mMatrix;
    private Taxi[] taxiList = new Taxi[100];
    private ArrayList<OutputSys> reList = new ArrayList<OutputSys>();
    private int numReq = 0;

    public InputSys(int map[][], int matrix[][]) {
        this.map = map;
        this.mMatrix = matrix;
    }

    
    public void Scheduler() {
        
        for (int i = 0; i < 100; i++) {
            taxiList[i] = new Taxi(this.map, this.mMatrix, i);
            new Thread(taxiList[i], "出租车" + i + "号").start();
        }

        //输入
        try (
                InputStreamReader reader = new InputStreamReader(System.in);
                BufferedReader br = new BufferedReader(reader)) {
            String myrequest = null;
            while ((myrequest = br.readLine()) != null) {
                if (myrequest.equals("exit")) {
                    System.exit(1);
                }
                myrequest = myrequest.replace(" ", "");
                long time = System.currentTimeMillis();
                Matcher matcher = PATTERN_NUMBER.matcher(myrequest);
                if (matcher.matches()) {
                        try {
                            String[] strings = myrequest.split(",");
                            int StartX = Integer.parseInt(strings[1].replace("(", ""));
                            int StartY = Integer.parseInt(strings[2].replace(")", ""));
                            int EndX = Integer.parseInt(strings[3].replace("(", ""));
                            int EndY = Integer.parseInt(strings[4].replace(")", "").replace("]", ""));

                            if (!isSame(StartX, StartY, EndX, EndY, time) && !isStill(StartX, StartY, EndX, EndY)) {
    
                                //系统调度线程
                                new Thread(new DispatchThread(this.taxiList, StartY, StartX, EndY, EndX, this.handlerTime(time), this.map), "调度线程" + (numReq++)).start();
                                
                            }
                        }catch (NumberFormatException e) {
                            System.out.println("输入格式错误！请重新输入");
                            System.exit(0);
                        }
                } else if (myrequest.equals("END")) {
                    System.exit(0);
                } else {
                    System.out.println("#invalid input");
                }
            }//while

        } catch (IOException e) {
            System.out.println("输入格式有误， 请重新输入！");
            System.exit(0);
        }

    }

   
    public long handlerTime(long time) {
        return time / 100 * 100;
    }

    public boolean isSame(int StartX, int StartY, int EndX, int EndY, long time) {
        OutputSys r = new OutputSys(StartX, StartY, EndX, EndY, handlerTime(time));
        boolean is = true;
        if (reList.isEmpty()) {
            is = false;
            reList.add(r);
        } else {
            for (int i = 0; i < reList.size(); i++) {
                OutputSys tmpR = reList.get(i);
                if (r.time == tmpR.time) {
                    if (r.equals(tmpR)) {
                        System.out.println("#same req"+r.EndX);
                        break;
                    } else {
                        reList.add(r);
                        is = false;
                        break;
                    }
                } else {
                    reList.clear();
                    reList.add(r);
                    is = false;
                }
            }
        }
        return is;
    }

    public boolean isStill(int StartX, int StartY, int EndX, int EndY) {
        boolean is = false;
        if((StartX == EndX) && (StartY == EndY)) {
            System.out.println("#rolling still!");
            is = true;
        }
        return is;
    }

}