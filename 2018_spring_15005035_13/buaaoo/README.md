## 被测试的方法：
*  inputHandler() 处理犯法输入
*  isTimeConflict() 判断时间乱序
*  add_req() 加入请求队列
*  get_req() 获得请求队列
*  myscheduler() 调度函数

## 覆盖率
* 语句覆盖率：90% (因为有一些函数处理的不是很好，并没有完全重构该函数，所以导致单元测试不能顺利进行)

* 分支覆盖率： 80%(对于一斤进行测试的函数，实现了基本的全覆盖)
* bug：第三次作业中的主要在输入输出格式处理以及调度函数上出问题，复现的bug已经被修改
* 测试用例数目：对于么一个测试分支，分别有一个正确的样例和一个错误的样例。
