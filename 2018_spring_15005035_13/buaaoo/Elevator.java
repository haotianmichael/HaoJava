package buaaoo;


public class Elevator implements EleMoving{
	
	public Elevator() {
		
	}
	
	private int Curfloor = 1;

	/**
	 *up --> true
	 *down --> false
	 */
	private boolean Curdirection = true;
	
    private boolean Curstatus = false;
	
	public int getfloor() {
		return this.Curfloor;
	}
	
	public boolean getdir() {
		return this.Curdirection;
	}
	
	@Override
	public void setEle(int flo, boolean isD, boolean isS) {
			this.Curdirection = isD;
			this.Curstatus = isS;
			this.Curfloor = flo;
	}	
	
	@Override
	public void setDir(boolean is ) {
		this.Curdirection = is;
	}
	
	
}
