package buaaoo;

import java.util.ArrayList;

public class PassingScheduler {
	
	private double sub_standard = 0;
	private Elevator sub_ele = new Elevator();
	public ArrayList<Request> sub_req= new ArrayList<Request>();
	
	public PassingScheduler(Elevator e, ArrayList<Request> r) {
		this.sub_ele = e;
		this.sub_req = r;
	}



	public void myscheduler()
	/**
	 * @REQUIRES:this.sub_req!= null
	 * @MODIFIES:this.sub_req
	 * @EFFECTS:
	 *      (\all double i; 0<=i<this.sub_req.length(); (this.sub_standard = \old(this.sub_standard) + sub_req.getTime()))
	 *      (\exist int i; i == this.sub_req.length() - 1; (this.ele.curStatus = (this.sub_req.getStatus)))
	 *      (\exist int i; i == this.sub_req.length() - 1; (this.ele.curdirection = (this.sub_req.getdir())))
	 *      this.sub_req.size() = 0
	 */
	{
		int num = sub_req.size();
		Request r = null;
		if(num == 0) {
			System.out.println("#INPUT INVALID");
			return;
		}
		while(!sub_req.isEmpty()) {
			r = sub_req.get(0);
			if(r.GetType()) {
				/*
				 * 	电梯指令元素： 时间 方向  状态
				 *
				 * */
				boolean dir = r.GetDir();
				boolean type_  = r.GetType();
				double time = r.GetTime();

				if(num == sub_req.size()) {
					if(r.GetFloor() == sub_ele.getfloor()) {
						this.sub_standard += 1;
						System.out.print("[ER,"+r.GetFloor()+","+r.GetTime()+"]/");
						System.out.println("("+r.GetFloor()+",STILL,"+this.sub_standard+")");
					}else {
						this.sub_standard = (r.GetFloor() - 1)*0.5 + 1.0;  //第一次时间
                        sub_ele.setEle(r.GetFloor(), true, false);
						System.out.print("[ER,"+r.GetFloor()+","+r.GetTime()+"]/");
						if(sub_ele.getdir()) {
							System.out.println("("+r.GetFloor()+",UP,"+(this.sub_standard - 1)+")");
						}else {
							System.out.println("("+r.GetFloor()+",DOWN,"+(this.sub_standard - 1)+")");
						}
					}
				}else {
					this.sub_standard = (this.sub_standard > r.GetTime()) ? this.sub_standard : r.GetTime();
					if(r.GetFloor() == sub_ele.getfloor()) {
						this.sub_standard += 1;
						System.out.print("[ER,"+r.GetFloor()+","+r.GetTime()+"]/");
						System.out.println("("+r.GetFloor()+",STILL,"+this.sub_standard+")");
					}else {

						if(sub_ele.getfloor() < r.GetFloor()) {
							this.sub_standard += (r.GetFloor() -sub_ele.getfloor())*0.5 + 1.0;
						}else {
							this.sub_standard += (sub_ele.getfloor() - r.GetFloor())*0.5 + 1.0;
						}

                        if(sub_ele.getfloor() < r.GetFloor()) {
                            sub_ele.setEle(r.GetFloor(), true, false);
                        }else {
                            sub_ele.setEle(r.GetFloor(), false, false);
                        }
						System.out.print("[ER,"+r.GetFloor()+","+r.GetTime()+"]/");
						if(sub_ele.getdir()) {
							System.out.println("("+r.GetFloor()+",UP,"+ (this.sub_standard - 1)+")");
						}else {
							System.out.println("("+r.GetFloor()+",DOWN,"+( this.sub_standard - 1)+")");
						}
					}
				}

				boolean type = sub_req.get(0).GetType();
				sub_req.remove(0);


				//同质过滤
				int i=0;
				while(i < sub_req.size()){
					double	circleTime = sub_req.get(i).GetTime();
					if(circleTime <= this.sub_standard) {
						if((sub_ele.getfloor() == sub_req.get(i).GetFloor()) && sub_req.get(i).GetType() ){
							System.out.println("#SAME("+"ER,"+sub_req.get(i).GetFloor()+","+sub_req.get(i).GetTime()+")");
							sub_req.remove(i);
							num--;
							i = 0;
						}else  {
							i++;
						}
					}else {
						i++;
					}
				}

				//捎带请求
				//passing(type);

			}else {
				//外部指令
				if(num == sub_req.size()) {
					if(r.GetFloor() == sub_ele.getfloor()) {
						this.sub_standard += 1;
						if(r.GetDir()) {
							System.out.print("[FR,"+r.GetFloor()+",UP,"+r.GetTime()+"]/");
						}else {
							System.out.print("[FR,"+r.GetFloor()+",DOWN,"+r.GetTime()+"]/");
						}
						System.out.println("("+r.GetFloor()+",STILL,"+this.sub_standard+")");
					}else {
						this.sub_standard = (r.GetFloor() - 1)*0.5 + 1.0;  //第一次时间
                        sub_ele.setEle(r.GetFloor(), true, false);
						if(r.GetDir()) {
							System.out.print("[FR,"+r.GetFloor()+",UP,"+r.GetTime()+"]/");
						}else {
							System.out.print("[FR,"+r.GetFloor()+",DOWN,"+r.GetTime()+"]/");
						}
						if(sub_ele.getdir()) {
							System.out.println("("+r.GetFloor()+",UP,"+(this.sub_standard - 1)+")");
						}else {
							System.out.println("("+r.GetFloor()+",DOWN,"+(this.sub_standard - 1)+")");
						}
					}
				}else {
					this.sub_standard = (this.sub_standard > r.GetTime()) ? this.sub_standard : r.GetTime();
					if(r.GetFloor() == sub_ele.getfloor()) {
						this.sub_standard += 1;
                        if(r.GetType()) {
                            sub_ele.setDir(true);
                        }else {
                            sub_ele.setDir(false);
                        }
						if(r.GetDir()) {
							System.out.print("[FR,"+r.GetFloor()+",UP,"+r.GetTime()+"]/");
						}else {
							System.out.print("[FR,"+r.GetFloor()+",DOWN,"+r.GetTime()+"]/");
						}
						System.out.println("("+r.GetFloor()+",STILL,"+this.sub_standard+")");
					}else {

						if(sub_ele.getfloor() < r.GetFloor()) {
							this.sub_standard += (r.GetFloor() -sub_ele.getfloor())*0.5 + 1.0;
                            sub_ele.setEle(r.GetFloor(), true, false);
						}else {
							this.sub_standard += (sub_ele.getfloor() - r.GetFloor())*0.5 + 1.0;
							sub_ele.setEle(r.GetFloor(), false, false);
						}

						if(r.GetDir()) {
							System.out.print("[FR,"+r.GetFloor()+",UP,"+r.GetTime()+"]/");
						}else {
							System.out.print("[FR,"+r.GetFloor()+",DOWN,"+r.GetTime()+"]/");
						}

						if(sub_ele.getdir()) {
							System.out.println("("+r.GetFloor()+",UP,"+(this.sub_standard - 1)+")");
						}else {
							System.out.println("("+r.GetFloor()+",DOWN,"+(this.sub_standard - 1)+")");
						}
					}
				}
				boolean type = this.sub_req.get(0).GetDir();
				sub_req.remove(0);


				//同质过滤
				int i=0;
				while(i < sub_req.size()){
					double circleTime = sub_req.get(i).GetTime();
					if(circleTime <= this.sub_standard) {
						if ((sub_ele.getfloor() == sub_req.get(i).GetFloor()) && !sub_req.get(i).GetType() && type == sub_req.get(i).GetDir()) {
							System.out.println("#SAME(" + "FR," + sub_req.get(i).GetFloor() + "," + sub_req.get(i).GetTime() + ")");
							sub_req.remove(i);
							num--;
							i = 0;
						}else {
							i++;
						}
					}else {
						i++;
					}

				}

				//passing(type);  //捎带请求

			}
		}
	}



	public void passing(boolean type)
	/**
	 * @REQUIRES:this.sub_req != null
	 * @MODIFIES:NONE
	 * @EFFECTS:
	 *       (\result = this) && （this.sub_req.length() = \old(this.sub_req.length() - 1)）
	 */
	{
		int i=0;
		Request re = null;
		while(i < sub_req.size()) {
			re = sub_req.get(i);
			if(!type) {
				if(this.judgingMoveFR(re.GetTime(), re.GetFloor(), re.GetDir(), re.GetType())) {
					sub_req.remove(i);						
				}else {
					i++;
				}				
			}else {
				if(this.judgeMoveER(re.GetTime(),re.GetFloor(), re.GetDir(), re.GetType())) { 
					sub_req.remove(i);
				}else {
					i++;
				}				
			}
			
		}
	}
	

	public boolean judgingMoveFR(double time, int floor, boolean dir, boolean type)
	/**
	 * @REQUIRES:this.sub_req != null
	 * @MODIFIES: NONE
	 * @EFFECTS:
	 *    (\result = true) ==> (dir == false) && (sub_ele.getfloor < this.floor) && (floor < Currentflo)
	 */
	{
		boolean is = false;					
		if((time < this.sub_standard) && !type) {
			//说明电梯是运动的
			double  tmp = this.sub_standard - 1 - time;
			int Currentflo = 0 ;
			if(dir == true) {
				 Currentflo = sub_ele.getfloor() - (int)(tmp/0.5);
			}else {
				 Currentflo = sub_ele.getfloor() + (int)(tmp/0.5);
			}
			if(dir == sub_ele.getdir()) {
				if(dir == false  && floor > sub_ele.getfloor() && floor < Currentflo) {
					this.sub_standard += 1;
					sub_ele.setEle(floor, false, false);
					is = true;
				}else if(dir == true && floor < sub_ele.getfloor() && floor > Currentflo) {
					this.sub_standard += 1;
					sub_ele.setEle(floor, true, false);
					is = true;
				}
			}			
		}else if (type && (time == this.sub_standard)) {
			
			if(floor < sub_ele.getfloor() && sub_ele.getdir()) {
				this.sub_standard += 1;
				is = true;
			}else if(floor > sub_ele.getfloor() && !sub_ele.getdir()) {
				this.sub_standard += 1;
				is = true;
			}
		}	
		return is;
	}
	
	
	public boolean judgeMoveER(double time, int floor, boolean dir, boolean type)
	/**
	 * @REQUIRES:this.sub_req != null
	 * @MODIFIES: NONE
	 * @EFFECTS:
	 *       （\result = true) ==> (dir == false) && (sub_ele.getfloor < this.floor) && (floor < Currentflo)
	 */
	{
		boolean is = false;
		if((time < this.sub_standard) && !type) {
			if(dir == false && floor < sub_ele.getfloor()) {
				this.sub_standard += 1;
				is = true;
			}else if (dir == true && floor > sub_ele.getfloor()) {
				this.sub_standard += 1;
				is = true;
			}			
		}		
		return is;
	}


}
