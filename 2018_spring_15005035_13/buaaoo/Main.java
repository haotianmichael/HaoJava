package buaaoo;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    
    static Pattern NUMBER_PATTERN = Pattern.compile("(\\(FR,(([2-9],(DOWN|UP))|(1,UP)|(10,DOWN)),\\+?0*[0-9]{0,10}\\))$|(\\(ER,([1-9]|10),\\+?0*[0-9]{0,10}\\))$");
    
    public static void main(String[] args) {
        RequestQueue request_list = new RequestQueue();
        Scanner input = new Scanner(System.in);
        String req = input.nextLine().replaceAll(" ", "");
        int i = 0;
        while(!req.equals("RUN") && (i++)<100) {
            inputHandler(request_list, req);
            req = input.nextLine().replaceAll(" ", "");
        }
        Elevator myele = new Elevator();
        PassingScheduler mysch = new PassingScheduler(myele, request_list.get_que());
        mysch.myscheduler();
        input.close();
    }
    
    
    public static boolean inputHandler(RequestQueue request_list, String req)
    /**
     * @REQUIRES:NONE
     * @MODIFIES:request_list
     * @EFFECTS:
     *          (\ all String str ; str.matcher ()) ==> (request_list.length() = \old(request_list.length()) + 1)
     *          (\exists String str; str.equals("RUN")) ==> breakl
     *          (or \result = "invalid [req]")
     */
    {
        int ii = 0;
        Matcher matcher1 = NUMBER_PATTERN.matcher(req);
        
        if (matcher1.find()) {
            request_list.add_req(req);
            return true;
            
        } else {
            System.out.println("INVALID" + "[" + req + "]");
            return false;
        }
        
    }
    
    
}