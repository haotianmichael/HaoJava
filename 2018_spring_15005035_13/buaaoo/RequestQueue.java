package buaaoo;

import java.util.ArrayList;

public class RequestQueue {
    
    private ArrayList<Request> req_list = new ArrayList<Request>();
    
    private ArrayList<Double> time = new ArrayList<Double>();
    
    public boolean isTimeConflict(double time)
    /**
     * @REQUIRES:(\all double time; time >=0)
     * @MODIFIES:NONE
     * @EFFECTS:
     *           (\ old ( this).time.isEmpty() && time != 0) ==> (\result = false)
     *           (\old(this).time.isEmpty() && time == 0) ==> (result = true)
     *           (\all int i, j; 0<=i < j < time.length(); time[i] < time[j]) ==> (\result = true)
     */
    {
        boolean is = true;
        if (this.time.isEmpty()) {
            if (time != 0) {
                is = false;
            }
        } else if (this.time.get(this.time.size() - 1) > time) {
            is = false;
        }
        return is;
        
    }
    
    
    public void add_req(String str_req)
    /**
     * @REQUIRES:str_req != null
     * @MODIFIES:this.req_list && this.time
     * @EFFECTS:
     *          req_list.length() = \old(this.req_list.length()) + 1
     *          time.length() = \old(this.time.length()) + 1
     */
    {
        String[] strs = str_req.split(",|\\(|\\)");
        String erinner = "ER";
        String frinner = "FR";
        if(erinner.equals(strs[1])) {
            long tmptime = Long.parseLong(strs[3]);
            if (tmptime > Math.pow(2.0, 32.0) - 1) {
                System.out.println("INVALID" + "[" + str_req + "]");
                return;
            }
        } else if (frinner.equals(strs[1])) {
            long tmptime = Long.parseLong(strs[4]);
            if (tmptime > Math.pow(2.0, 32.0) - 1) {
                System.out.println("INVALID" + "[" + str_req + "]");
                return;
            }
        }
        
        if (strs[1].equals("ER") && isTimeConflict(Double.parseDouble(strs[3]))) {
            Request req = new Request(str_req, true);
            req_list.add(req);
            time.add(Double.parseDouble(strs[3]));
        } else if (strs[1].equals("FR") && isTimeConflict(Double.parseDouble(strs[4]))) {
            boolean isup = (strs[2].equals("1") && strs[3].equals("DOWN"));
            boolean isdown = (strs[2].equals("10") && strs[3].equals("UP"));
            if (isup || isdown) {
                System.out.println("INVALID" + "[" + str_req + "]");
            } else {
                Request req = new Request(str_req, false);
                req_list.add(req);
                time.add(Double.parseDouble(strs[4]));
            }
        } else {
            System.out.println("INVALID" + "[" + str_req + "]");
        }
        
    }
    
    public ArrayList<Request> get_que() {
        return this.req_list;
    }
    
    
   
    
}
