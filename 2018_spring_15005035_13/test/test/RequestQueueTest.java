package test;

import buaaoo.Request;
import buaaoo.RequestQueue;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class RequestQueueTest {
    
     RequestQueue req = new RequestQueue();
    
    @Test
    public void isTimeConflict()
    /**
     * if(req.isEmpty())，time = 0 ==> (\result = true)
     */
    {
        int timeA = 4;
        assertEquals(false, req.isTimeConflict(timeA));
        
    }
    
    @Test
    public void isTimeConflictB()
    /**
     * if(!req.isEmpty());
     * (\all int i, j); 0<=i < j < req.legnlth() - 1;
     * req.get(i).getTime() < req.get(j)getTime()
     */
    {
        String str = "(FR,3,UP,0)";
        String str1  = "(FR,6,DOWN,4)";
        req.add_req(str);
        req.add_req(str1);
    
        /**
         * time disOrder
         */
        int timeA = 3;
        assertEquals(false, req.isTimeConflict(timeA));
        //assertEquals(true, req.isTimeConflict(timeA));
    
    
        /**
         * time deOrder
         *
         *int timeB = 6;
         *assertEquals(true, req.isTimeConflict(timeB));
         *assertEquals(false, req.isTimeConflict(timeB));
         */
        
    }
    
    @Test
    public void add_req()
    /**
     * @REQUIRES: legal input
     * Bying adding  two elements to test
     */
    {
        String str = "(FR,3,UP,0)";
        String str1  = "(FR,6,DOWN,4)";
        req.add_req(str);
        req.add_req(str1);
        assertEquals(2, req.get_que().size());
    }
    
    @Test
    public void get_que()
    /**
     * que.length() == (\old)que.length()
     */
    {
        String str = "(FR,3,UP,0)";
        String str1  = "(FR,6,DOWN,4)";
        req.add_req(str);
        req.add_req(str1);
        
        ArrayList<Request> newReq;
        newReq = req.get_que();
        /**
         * "UP" --> true
         * "DOWN" --> false
         */
        assertEquals(false, newReq.get(1).GetDir());
    }
    
    
   
}