package buaaoo;

import java.util.ArrayList;
import java.util.Random;
import java.util.Stack;

public class Taxi implements Runnable {
    
    private int ID;
    private int[][] taxiMap;
    private int[][] taxiMatrix;
    /**
     * 出租车订单(-->乘客)的最短路径
     */
    private Stack<Integer> taxiPath = new Stack<>();
    
    /**
     * 出租车订单（-->目标）的最短路径
     */
    private Stack<Integer> taxiTerminalPath = new Stack<>();
    private int x;
    private int y;
    private state TaxiState;
    private int credit;
    
    public enum state {
        still, approaching, service, waiting;
    }
    
    private OutputSys taxiReqest;
    
    public Taxi(int[][] map, int[][] matrix, int ID) {
        this.taxiMap = map;
        this.taxiMatrix = matrix;
        Random random = new Random();
        x = random.nextInt(80);
        y = random.nextInt(80);
        credit = 0;
        this.ID = ID;
        this.TaxiState = state.waiting;
    }
    
    /**
     *系统与出租车订单的接口
     */
    public void updateReq(OutputSys taxiRequest, Stack<Integer> pathPas, Stack<Integer> pathTer) {
        this.taxiReqest = taxiRequest;
        this.taxiPath = pathPas;
        this.taxiTerminalPath = pathTer;
        this.taxiReqest.getTaxi(this.ID, this.x, this.y, System.currentTimeMillis());
    }
    
    public synchronized void setTaxiState(String name) {
        for (state e : state.values()) {
            if (e.toString().equals(name)) {
                this.TaxiState = e;
            }
        }
    }
    
    public synchronized String getState() {
        return this.TaxiState.toString();
    }
    
    public int getX() {
        return this.x;
    }
    
    public int getY() {
        return this.y;
    }
    
    
    public synchronized int getCredit() {
        return this.credit;
    }
    
    public int getID() {
        return this.ID;
    }
    
    public synchronized void setCredit(String Type) {
        if (Type.equals("passengers")) {
            this.credit = this.credit + 3;
        } else if (Type.equals("OutputSys")) {
            this.credit++;
        }
    }
    
    
    /*********出租车运动函数相关******/
    
    public void goEast() {
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        this.x++;
    }
    
    public void goWest() {
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        this.x--;
    }
    
    public void goSouth() {
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        this.y++;
    }
    
    public void goNorth() {
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        this.y--;
    }
    
    public void chooseDirection() {
        ArrayList<String> direction = new ArrayList<String>();
        
        
        if ((this.taxiMap[this.x][this.y] == 1) || (this.taxiMap[this.x][this.y] == 3)) {
            direction.add("south");
        } else if ((this.taxiMap[this.x][this.y] == 2) || (this.taxiMap[this.x][this.y] == 3)) {
            direction.add("east");
        }
        
        if (this.y != 0) {
            if ((this.taxiMap[this.x][this.y - 1] == 1) || (this.taxiMap[this.x][this.y - 1] == 3)) {
                direction.add("north");
            }
        }
        
        if (this.x != 0) {
            if ((this.taxiMap[this.x - 1][this.y] == 2) || (this.taxiMap[this.x - 1][this.y] == 3)) {
                direction.add("west");
            }
        }
        
        
        //开始随机分配
        Random random = new Random();
        int number = random.nextInt(direction.size());
        
        
        //确定行驶方向
        String confirmDir = direction.get(number);
        if (confirmDir.equals("east")) {
            this.goEast();
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
    
        } else if (confirmDir.equals("west")) {
            this.goWest();
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
    
        } else if (confirmDir.equals("south")) {
            this.goSouth();
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
    
        } else if (confirmDir.equals("north")) {
            this.goNorth();
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
    
        }
        
        
    }
    
    /***********出租车运动函数相关********/
    
   
    
    /**
     * approaching
     */
    public synchronized void taxiApproaching() {
        System.out.println("第" + this.ID + "号出租车成功接到订单！恭喜");
        
        while(!taxiPath.isEmpty()) {
            int next = taxiPath.peek();
            System.out.println(next);
            this.taxiRun(next);
            taxiPath.pop();
        }
        
    }
    
    /**
     * service
     */
    public synchronized void taxiService() {
        while(!taxiTerminalPath.isEmpty()) {
            int next = taxiTerminalPath.peek();
            this.taxiRun(next);
            taxiTerminalPath.pop();
        }
    }
    
    /**
     * taxiRun
     */
    public void taxiRun(int next) {
        int beg = this.taxiReqest.StartX *80 + this.taxiReqest.StartY;
        final int PRE_DIR = 80;
        if(next == (beg+1)) {
            this.goEast();
            System.out.println("east");
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }else if(next == (beg-1)) {
            this.goWest();
            System.out.println("west");
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }else if(next == (beg+PRE_DIR)) {
            this.goNorth();
            System.out.println("north");
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }else if(next == (beg-PRE_DIR)) {
            this.goSouth();
            System.out.println("south");
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Taxi) {
            if (this.ID == ((Taxi) obj).ID) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    
    @Override
    public void run() {
        
        long CurrentTime = System.currentTimeMillis();
        while (true) {
            if ((System.currentTimeMillis() - CurrentTime) >= 20000) {
                this.setTaxiState("still");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                this.setTaxiState("waiting");
                CurrentTime = System.currentTimeMillis();
            } else {
                //进入接单状态
                switch (this.TaxiState) {
                    case approaching:
                        synchronized (this) {
    
                            this.taxiApproaching();
                            this.taxiReqest.getPassengerTime(System.currentTimeMillis());
    
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            this.setTaxiState("service");
                            break;
                        }
                    case service:
                        {
                            this.taxiService();
                            this.taxiReqest.getArrail(System.currentTimeMillis());
                            
                            System.out.println("第" + this.ID + "号出租车成功完成订单！ 恭喜");
                            this.taxiReqest.print();
                            
                            this.setCredit("passenger");
                            this.setTaxiState("still");
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            this.setTaxiState("waiting");
                            break;
                        }
                    default:
                    {
                        this.chooseDirection();
                        break;
                    }
    
                }
            }
        }
    }
    
    
    
    
    
}
