package buaaoo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
	


public class Polymain {
	
	private Poly []myPoly = new Poly[20];//每一个具体的多项式
	private Poly finalPoly = new Poly();  //最后的结果多项式
	private int Polynumbers = 0;  //多项式的数目
	private String inputStr; //输入的多项式表达式
	
	public Polymain() {
		
	}
	
	//多项式分割函数
	public void splitString(String inputstr) {
		int num = 0, i = 0, j = 0;
		String []strings  = new String[20];
		while(i<inputstr.length()) {
			if(inputstr.substring(i, i+1).equals("}")) {			
				strings[num] = inputstr.substring(j, i+1);
				j = i+1;	
				this.myPoly[num] = new Poly(strings[num]);
				num++;
			}
			i++;
		}						
		this.Polynumbers = num;		 //多项式的项数
	}

	//多项式运算函数
	public void  PolyCaculate() {
		int i=0; 
		for(; i<Polynumbers; i++) {
			if(myPoly[i].getIsHead()) {
				finalPoly.addPoly(myPoly[i]);
			}else {
				finalPoly.subPoly(myPoly[i]);
			}
		}		
	}
	
	//结果输出函数
	public void PolyOutput(Poly finalPoly) {
	
		int tmpNumbers = finalPoly.getTermsNunbers();
		Terms []tmpTerms = new Terms[tmpNumbers];  //临时数组
		String [][]tmpSrray  = new String[tmpNumbers][2];  //临时排序数组
		int i=0; 
		//将finalPoly中的数对元素Copy过来(传指针)
		tmpTerms = finalPoly.getTermsEle();
		
		//得到最后finalPoly中的数对
		for(int j=0; i<tmpNumbers; i++) {
			tmpSrray[i][j] = String.valueOf(tmpTerms[i].getdegree());
			tmpSrray[i][j+1] = String.valueOf(tmpTerms[i].getcoeff());
		}
		
//		//对string数组进行排序
//		Arrays.sort(tmpSrray, new Comparator<String[]>(){            
//            public int compare(String[] o1, String[] o2) {
//                return o1[1].compareTo(o2[1]);
//            }
//        });
		
		for(int k=tmpNumbers-1; k>0; k--) {
			for(int l=0; l<k; l++) {
				if(Integer.parseInt(tmpSrray[l][1]) > Integer.parseInt(tmpSrray[l+1][1])) {
					String [][]temp = new String[1][2];
					temp[0] = tmpSrray[l];
					tmpSrray[l] = tmpSrray[l+1];
					tmpSrray[l+1] = temp[0];
				}
			}
		}
		
		//输出结果
		boolean isZero = true;
		for(int k=0; k<tmpNumbers; k++) {
			if(Integer.parseInt(tmpSrray[k][0]) != 0) {
				isZero = false;
			}
		}
		if(isZero) {
			System.out.println("0");
		}else if(!tmpSrray[tmpNumbers-1][0].equals("0")){
			System.out.print("{(");
			int l = 0;
			for(int k=0; k<tmpNumbers-1; k++) {
				if(!tmpSrray[k][l].equals("0")) {
					System.out.print(tmpSrray[k][l]+",");
					System.out.print(tmpSrray[k][l+1]+"),(");
				}
			}			
			System.out.print(tmpSrray[tmpNumbers-1][l]+",");
			System.out.println(tmpSrray[tmpNumbers-1][l+1]+")}");			
		}else {
			System.out.print("{(");
			int l = 0;
			for(int k=0; k<tmpNumbers-2; k++) {
				if(!tmpSrray[k][l].equals("0")) {
					System.out.print(tmpSrray[k][l]+",");
					System.out.print(tmpSrray[k][l+1]+"),(");
				}
			}	
			System.out.print(tmpSrray[tmpNumbers-2][l]+",");
			System.out.println(tmpSrray[tmpNumbers-2][l+1]+")}");		
		}		
	}
	
	//主函数
	public static void main(String[] args) {
					
		try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
		
			Polymain polymain = new Polymain();
			polymain.inputStr = br.readLine(); 
			//br.readLine()表示当用户从键盘上输入一行结束之后，用户刚刚输入的内容就会被br读取到
				String Polystring  = null;
				Polystring = polymain.inputStr.replace(" ", ""); 
				//正则表达式判断输入
				Matcher matcher = Pattern.compile("[+-]?\\{\\([+-]?(\\d){1,6},[+]?(\\d){1,6}\\)(,\\([+-]?(\\d){1,6},[+]?(\\d){1,6}\\)){0,49}\\}"
											   + "([+-]\\{\\([+-]?(\\d){1,6},[+]?(\\d){1,6}\\)(,\\([+-]?(\\d){1,6},[+]?(\\d){1,6}\\)){0,49}\\}){0,19}").matcher(Polystring);
				if(!matcher.matches()) {
					System.out.println("ERROR");
					System.out.println("#format error!");
					System.exit(0);
				}else {
					
					polymain.splitString(Polystring);
					
					polymain.PolyCaculate();
					
					polymain.PolyOutput(polymain.finalPoly);
				}
				
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
}
