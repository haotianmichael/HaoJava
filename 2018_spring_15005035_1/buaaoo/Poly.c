#include<stdio.h>

struct Poly{
  int degree = -1;
  int ceoff = -1;
};


Poly result[20];  //多项式的数组元素

//多项式分割函数
int splitString(char* a) {
  int i=0;
  int j = 0;
 
  while(a[i]) {
	int tmpDegree = 0;
	int tmpCeoff = 0;
	if(a[i] == '(') {
	  i++;
	  while(a[i] != ',') {
		tmpDegree *= 10;
		tmpDegree += (a[i]-'0');
		i++;
	  }
	  i++;	  
	  while(a[i] != ')'){
		tmpCeoff *= 10;
		tmpCeoff += (a[i]-'0');
		i++;
	  }
	  result[j].degree = tmpDegree;
	  result[j].ceoff = tmpCeoff;
	  j++;
	}
	i+=2;
  }
  return j;
}

//多项式运算函数
int  PolyCauclate(Poly *poly, int numbers) {

  int numberstmp = numbers;
  for(int i=0; i<numbers; i++) {
	for(int j=i+1; j<numbers; j++) {
	  if(poly[j].ceoff == poly[i].ceoff && poly[j].ceoff != -1) {
		poly[i].degree += poly[j].degree;
		poly[j].degree = -1;
		poly[j].ceoff = -1;
		numberstmp--;
	  }
	}
  }
  
  
  int result[numberstmp][2];
  int i=0;
  for(int j=0; j<numbers && i<numberstmp; j++) {
	if(poly[j].ceoff != -1) {
	  result[i][0] = poly[j].degree;
	  result[i][1] = poly[j].ceoff;
	  i++;
	}
  }
 
  //输出函数
  //result[][];
  printf("%s", "{(");
  for(int l=0; l<=numberstmp-2; l++) {
	printf("%d", result[l][0]);
	printf("%s", ",");
	printf("%d", result[l][1]);
	printf("%s", "),(");	  
  }

  printf("%d", result[numberstmp-1][0]);
  printf("%s", ",");
  printf("%d", result[numberstmp-1][1]);
  printf("%s\n", ")}");
	
}


/**
   1.输入字符串
   2.分割字符串
   3.计算字符串
   4.输出结果

*/

int main() {
  char *a;
  scanf("%s", a); //字符串输入
  int numbersPoly = splitString(a);  //将字符串转换成为标准的多项式
  PolyCauclate(result, numbersPoly);
  return 0;
  
}
