package buaaoo;
import java.util.regex.Pattern;
import java.util.regex.Matcher;


public class Poly {
	private boolean isHead;
	private int Termnumbers = 0;
	private Terms []terms = new Terms[50];
	private String Polystring;
	
	
	public Poly(String str) {		
		this.isHead = isPolyHead(str);
		Polystring = str;	
		initTerms();
	}
		
	public Poly() {
		this.isHead = true;    
		Polystring = null;
		Termnumbers = 0;
		int i=0;
		for(; i<50; i++) {
			terms[i] = new Terms(-1, -1);
		}
	}
	
	//符号判断函数
	public boolean isPolyHead(String strings) {
		
		boolean isHead = true;
		
		if(strings.substring(0,1).equals("+")) {
			isHead = true;
		}else if(strings.substring(0,1).equals("-")){
			isHead = false;
		}else  {
			isHead = true;
		}
		return isHead;
	}
	
	//项数初始化
	public void initTerms() {
		
		int i = 0, j = 0;
		Matcher matcher = Pattern.compile("[+-]?[0123456789]{1,6}").matcher(Polystring);
		int []regStr = new int[100]; //最多100
		while(matcher.find()) {
			regStr[j++] = Integer.parseInt(matcher.group());
		}		
		for(i=0; i<j; i+=2) {
			terms[Termnumbers] = new Terms(regStr[i], regStr[i+1]);
			Termnumbers++;
		}
		//判断是否有重复的多项数
		for(int f=0; f<Termnumbers-1; f+=2) {
			if(terms[f].getcoeff() == terms[f+1].getcoeff()) {
				System.out.println("ERROR");
				System.out.println("#same terms existing!");
				System.exit(0);
			}					
		}
		
		
	}
	
	//获得terms变量个数
	public int getTermsNunbers() {
		return this.Termnumbers;
	}
	
	//获得多项式数组元素
	public Terms[] getTermsEle() {
		return this.terms;		
	}
	
	//获得isHead变量
	public boolean getIsHead() {
		return isHead;
	}
	
	//改变isHead变量
	public void setIsHead(boolean is) {
		isHead = is;
	}
	
	/*两个多项式相加
	 * 结果放在第一个加数中
	*/
	public Poly addPoly(Poly adder) {
		
		int i = 0;	
		
		final int adder_nunmbers = adder.Termnumbers;
		for(; i<adder_nunmbers; i++) {
		
			boolean flag = true;  //标志变量，默认为新添加的项数
			for(int j=0; j<this.Termnumbers; j++) {
				if(this.terms[j].getcoeff() == adder.terms[i].getcoeff()) {
					int tmp = this.terms[j].getdegree() + adder.terms[i].getdegree();
					this.terms[j].setdegree(tmp); //系数相同的多项式相加
					flag = false;
				}		
			}
			if(flag) {
				//如果第二个加数中的某一个项数没有与之相匹配的第一个项数，那么在第一个项数后面在申请该数组元素
				this.terms[Termnumbers] = new Terms(adder.terms[i].getdegree(), 
				 										adder.terms[i].getcoeff());
				this.Termnumbers++;
			}				
		}				
		return this;
	}
	
	//两个多项式相减
	public Poly subPoly(Poly subtractor) {
		int i =0; 
		final int sub_numbers = subtractor.Termnumbers;
		for(; i<sub_numbers; i++) {
			boolean flag = true;
			for(int j=0; j<this.Termnumbers; j++) {
				if(this.terms[j].getcoeff() == subtractor.terms[i].getcoeff()) {
					int tmp = this.terms[j].getdegree() - subtractor.terms[i].getdegree();
					this.terms[j].setdegree(tmp);
					flag = false;
				}
			}
			if(flag) {
				this.terms[Termnumbers] = new Terms(-(subtractor.terms[i].getdegree()), 
														subtractor.terms[i].getcoeff());
				Termnumbers++;
			}
		}
		return this;
	}
}
