package buaaoo;

public class Request {

	private boolean Type;   //指令类型
	private double time;   ///指令的发出时间
	private int floor;    //指令涉及到的楼层
	private boolean direction;  //指令的方向

	//构造方法
	public Request(String string, boolean is) {
		String []s;
		s = string.split(",|\\(|\\)");
		this.Type = is;
		this.floor = Integer.parseInt(s[2]);
		if(Type) {
			//内部指令
			this.time = Double.parseDouble(s[3]);
		}else {
			//外部指令
			this.time = Double.parseDouble(s[4]);
			if(s[3].equals("UP")) {
				this.direction = true;
			}else if(s[3].equals("DOWN")){
				this.direction = false;
			}
		}

	}

	//获取元素
	public boolean GetType() {
		return this.Type;
	}

	public double GetTime() {
		return this.time;
	}

	public int GetFloor() {
		return this.floor;
	}

	public boolean GetDir() {
		return this.direction;
	}
}
