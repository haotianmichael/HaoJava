package buaaoo;

import java.util.ArrayList;

public class RequestQueue {
	
	//命令队列
	private ArrayList<Request> req_list= new ArrayList<Request>();
	
	//时间队列(判断时间冲突)
	private ArrayList<Double> time = new ArrayList<Double>();
	
	
	
	//判断时间（1.乱序输入  2.第一条指令时间不为零）
	public boolean isTimeConflict(double time) {
		boolean is = true;
		if(this.time.isEmpty()) {
			if(time != 0) {
				is = false;
			}
		}else if(this.time.get(this.time.size() - 1) > time) {
			is = false;			
		}
		return is;
	}

	//加入命令队列
	public void add_req(String str_req) {

		//处理命令的执行时间超过预定范围
			 String[] strs = str_req.split(",|\\(|\\)");
			if(strs[1].equals("ER")) {
				long tmptime = Long.parseLong(strs[3]);
				if(tmptime > Math.pow(2.0, 32.0) - 1) {
					System.out.println("INVALID" + "[" + str_req + "]");
					return;
				}
			}else if(strs[1].equals("FR")) {
				long tmptime = Long.parseLong(strs[4]);
				if(tmptime > Math.pow(2.0, 32.0) - 1) {
					System.out.println("INVALID" + "[" + str_req + "]");
					return;
				}
			}

			if (strs[1].equals("ER") && isTimeConflict(Double.parseDouble(strs[3]))) {
				//电梯指令
				Request req = new Request(str_req, true);
				req_list.add(req);
				time.add(Double.parseDouble(strs[3]));
			} else if (strs[1].equals("FR") && isTimeConflict(Double.parseDouble(strs[4]))) {
				//电梯外部请求
				if ((strs[2].equals("1") && strs[3].equals("DOWN")) ||
						(strs[2].equals("10") && strs[3].equals("UP"))) {
					System.out.println("INVALID" + "[" + str_req + "]");
				} else {
					Request req = new Request(str_req, false);
					req_list.add(req);
					time.add(Double.parseDouble(strs[4]));
				}
			} else {
				System.out.println("INVALID" + "[" + str_req + "]");
			}

	}

	//获得命令队列
	public ArrayList<Request> get_que() {
		return this.req_list;
	}
}
