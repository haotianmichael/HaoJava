package buaaoo;

import java.util.ArrayList;

public class Scheduler {

	private double standard = 0;
	private Elevator ele = new Elevator();
	private ArrayList<Request> req= new ArrayList<Request>();

	public Scheduler() {

	}

	public Scheduler(Elevator e, ArrayList<Request> r) {
		this.ele = e;
		this.req = r;
	}

	public void myscheduler() {
		int num = req.size();
		Request r = null;
		if(num == 0) {
			System.out.println("#Input invalid");
			return;
		}
		while(!req.isEmpty()) {
			r = req.get(0);
			if(r.GetType()) {
				//电梯指令
				if(num == req.size()) {
					if(r.GetFloor() == ele.getfloor()) {
						this.standard += 1;
						System.out.println("#SAME");
					}else {
						this.standard = (r.GetFloor() - 1)*0.5 + 1.0;  //第一次时间
						ele.setEle(r.GetFloor(), true, false);
					}
				}else {
					this.standard = (this.standard > r.GetTime()) ? this.standard : r.GetTime();
					if(r.GetFloor() == ele.getfloor()) {
						this.standard += 1;
					}else {
						this.standard += (r.GetFloor() -1)*0.5 + 1.0;
						if(ele.getfloor() < r.GetFloor()) {
							ele.setEle(r.GetFloor(), true, false);
						}else {
							ele.setEle(r.GetFloor(), false, false);
						}
					}
				}
				req.remove(0);


				int i=0;
				while(i < req.size()){
					double	circleTime = req.get(i).GetTime();
					if(circleTime <= this.standard) {
						if((ele.getfloor() == req.get(i).GetFloor()) && req.get(i).GetType() ){
							System.out.println("#SAME("+"ER,"+req.get(i).GetFloor()+","+req.get(i).GetTime()+")");
							req.remove(i);
							num--;
							i = 0;
						}
					}
					i++;
				}
			}else {
				//外部指令
				if(num == req.size()) {
					if(r.GetFloor() == ele.getfloor()) {
						this.standard += 1;
					}else {
						this.standard = (r.GetFloor() - 1)*0.5 + 1.0;  //第一次时间
						ele.setEle(r.GetFloor(), true, false);
					}
				}else {
					this.standard = (this.standard > r.GetTime()) ? this.standard : r.GetTime();
					if(r.GetFloor() == ele.getfloor()) {
						this.standard += 1;
						if(r.GetType()) {
							ele.setDir(true);
						}else {
							ele.setDir(false);
						}
					}else {
						this.standard += (r.GetFloor() -1)*0.5 + 1.0;
						if(ele.getfloor() < r.GetFloor()) {
							ele.setEle(r.GetFloor(), true, false);
						}else {
							ele.setEle(r.GetFloor(), false, false);
						}
					}
				}
				req.remove(0);
				num--;

				int i=0;
				while(i < req.size()){
					double circleTime = req.get(i).GetTime();
					if(circleTime <= this.standard) {
						if((ele.getfloor() == req.get(i).GetFloor()) && !req.get(i).GetType() ){
							req.remove(i);
							System.out.println("#SAME("+"ER,"+req.get(i).GetFloor()+","+req.get(i).GetTime()+")");
							num--;
							i = 0;
						}
					}
					i++;
				}//while
			}//else
		}//while
	}//function


}
