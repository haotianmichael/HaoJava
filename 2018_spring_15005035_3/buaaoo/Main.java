package buaaoo;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
	
	
	public static void main(String[] args) {
		int ii = 0;
		Scanner input = new Scanner(System.in);
		RequestQueue request_list = new RequestQueue();
		String req = null;
		Pattern pattern1 = Pattern.compile("(\\(FR,(([2-9],(DOWN|UP))|(1,UP)|(10,DOWN)),\\+?0*[0-9]{0,10}\\))$|(\\(ER,([1-9]|10),\\+?0*[0-9]{0,10}\\))$");

		while(true) {
 			//正则表达式匹配非法字符
			req = input.nextLine().replaceAll(" ", "");			
 			Matcher matcher1 = pattern1.matcher(req);
 			
 			if(matcher1.find()) { 				
 				request_list.add_req(req);	
 				
 				if(ii++ > 100) {
 					System.out.println("#INPUT INVALID");
 					return;
 				}
 			}else if(req.equals("RUN")) {
 				break;
 			}else {
 				System.out.println("INVALID"+"[" + req + "]");
			}
 					
		}
		Elevator myele = new Elevator();
	 	PassingScheduler mysch = new PassingScheduler(myele, request_list.get_que());
	 	
		//调度  ---  输出
		mysch.myscheduler();


	 	input.close();	
	}
}
