package buaaoo;

public class Request {
    
    private boolean Type;
    private double time;
    private int floor;
    private boolean direction;
    
    public Request() {
    
    }
    
    public Request(String string, boolean is)
    /**
     * @REQUIRES:string!=null;
     * @MODIFIES:this
     * @EFFECTS:
     *         （\result  = this） && （this.time != 0) && (this.floor != 0) && (this.Type == is) && (this.direction != null)
     */
    {
        String[] s;
        s = string.split(",|\\(|\\)");
        this.Type = is;
        this.floor = Integer.parseInt(s[2]);
        if (Type) {
            //内部指令
            this.time = Double.parseDouble(s[3]);
        } else {
            //外部指令
            this.time = Double.parseDouble(s[4]);
            if (s[3].equals("UP")) {
                this.direction = true;
            } else if (s[3].equals("DOWN")) {
                this.direction = false;
            }
        }
        
    }
    
    public boolean GetType() {
        return this.Type;
    }
    
    public double GetTime() {
        return this.time;
    }
    
    public int GetFloor() {
        return this.floor;
    }
    
    public boolean GetDir() {
        return this.direction;
    }
}
