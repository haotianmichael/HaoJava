package buaaoo;

import java.awt.*;
import java.util.*;

public class DispatchThread implements  Runnable{

    private static int PRE_TIME = 3000;
    /**
     * 邻接矩阵
     */
    private int [][]map;
    /**
     * 保存了从x到y的最短路径
     */
    private int [][]Dis = new int[6405][6405];
    /**
     * 保留地图中任意一点到root点的最短路径
     */
    private int[] edgeTo = new int[6400];
    /**
     * 选中的出租车(-->乘客)的路线
     */
    private Stack<Integer> pathPassenger = new Stack<>();
    
    /**
     * 选中的出租车（-->目标地）的路线
     */
    private Stack<Integer> pathTerminal = new Stack<>();
    
    private Taxi[] taxiList;
    private ArrayList<Taxi> taxiRequired = new ArrayList<Taxi>();

    private int StartX;
    private int StartY;
    private int EndX;
    private int EndY;
    private long time;

    public DispatchThread(Taxi[] taxiList, int StartX, int StartY, int EndX, int EndY, long time, int map[][]) {
        this.taxiList = taxiList;  //每一个调度线程都要有个地图
        this.StartX = StartX;
        this.StartY = StartY;
        this.EndX = EndX;
        this.EndY = EndY;
        this.time = time;
        this.map = map;
        //对距离矩阵赋初值
        for(int i=0; i<6400; i++){
            for(int j=0; j<6400; j++){
                if(i == j) {
                    this.Dis[i][j] = 0;
                }else {
                    this.Dis[i][j] = map[i][j];
                }
            }
        }
        
    }

    /**
     * 系统调度函数
     */
    public void Simulator() {

        long StartTime = System.currentTimeMillis();
        
        System.out.println("---抢单中---");
        while(((System.currentTimeMillis() - StartTime))<PRE_TIME && (this.FliterTaxi())){}
        System.out.println("----抢单结束---");

        this.ConfirmTaxi();
    }

    public boolean FliterTaxi() {
        int XLeft = StartX - 2;
        int Xright = (StartX + 2) >= 79 ? 79 : (StartX + 2);
        int YLeft = StartY - 2;
        int Yright = (StartY + 2) >= 79 ? 79 : (StartY + 2);
        for(int i=0; i<100; i++) {
            if(taxiList[i].getState().equals("waiting") && (taxiList[i].getX()<=Xright && taxiList[i].getX()>=XLeft)
                    &&(taxiList[i].getY()<=Yright && taxiList[i].getY()>=YLeft)) {
                boolean is = false;
                for(int k = 0; k<taxiRequired.size(); k++) {
                    if(taxiRequired.get(k).equals(taxiList[i])) {
                        is = true;
                        break;
                    }
                }
                if(!is) {
                    taxiList[i].setCredit("OutputSys");
                    taxiRequired.add(taxiList[i]);
                    System.out.println(taxiList[i].getID()+ "号出租车抢单成功！ 等待系统调度中");
                }
            }
        }
        return true;
    }

    public void ConfirmTaxi() {
        Comparator<Taxi> OrderCredit = new Comparator<Taxi>() {

            @Override
            public int compare(Taxi o1, Taxi o2) {
                return  (o2.getCredit() - o1.getCredit());
            }
        };
        Queue<Taxi> filterTaxiFir = new PriorityQueue<>(OrderCredit);

        if(taxiRequired.isEmpty()) {
            System.out.println("#No Taxi available");
        }else {

            for(int j=0; j<taxiRequired.size(); j++) {
               filterTaxiFir.add(taxiRequired.get(j));
            }
            OutputSys RequestConfired  = new OutputSys(StartX, StartY, EndX, EndY, time, this.taxiRequired);

            if(filterTaxiFir.size() == 1) {
                System.out.println("只有"+filterTaxiFir.element().getID()+"号出租车接到订单");
                this.toPath(filterTaxiFir.element().getX(), filterTaxiFir.element().getY(), StartX, StartY, this.pathPassenger);
                this.toPath(StartX, StartY, EndX, EndY, this.pathTerminal);
                filterTaxiFir.element().updateReq(RequestConfired, this.pathPassenger, this.pathTerminal);
                filterTaxiFir.element().setTaxiState("approaching");

            }else {
                //开始选择信用度最高的队列
                System.out.println("有多辆出租车， 分别是：");
                for(int i=0; i<taxiRequired.size(); i++) {
                    System.out.println(taxiRequired.get(i).getID()+"号出租车");
                }
                System.out.println("开始调度出租车队列");
                ArrayList<Taxi> filter = new ArrayList<Taxi>();
                //单元最短路径
                filter.add(filterTaxiFir.peek());
                filterTaxiFir.poll();
                while(!filterTaxiFir.isEmpty() && filter.get(0).getCredit() == filterTaxiFir.peek().getCredit()){
                    filter.add(filterTaxiFir.peek());
                    filterTaxiFir.poll();
                }
                
                //信用选择结束，开始判断最短路径(随机算法是最后一个相等的出租车)
                int dis = Dis(filter.get(0).getX(), filter.get(0).getY(), StartX, StartY);
                int taxiNo = 0;
                for(int i=1; i<filter.size(); i++) {
                    if(Dis(filter.get(i).getX(), filter.get(i).getY(), StartX, StartY) <= dis && filter.get(i).getState().equals("waiting")) {
                        taxiNo = i;
                        dis = Dis(filter.get(i).getX(), filter.get(i).getY(), StartX, StartY);
                    }
                }
                
                //过滤结束，制定出租车开始运动
                System.out.println(filter.get(taxiNo).getID()+" 号出租车调度成功！");
                //出租车到乘客的最短路径
                this.toPath(filter.get(taxiNo).getX(), filter.get(taxiNo).getY(), StartX, StartY, this.pathPassenger);
                this.toPath(StartX, StartY, EndX, EndY, this.pathTerminal);
                filter.get(taxiNo).updateReq(RequestConfired, this.pathPassenger, this.pathTerminal);
                filter.get(taxiNo).setTaxiState("approaching");

            }
        }
    }

  

    public void bfs(int root) {
        
        int[] offset = new int[] {0, -1, 1, -80, 80}; //方向
        boolean[] marked = new boolean[6405];// 标志数组
      
        /**
         * 这里使用Point来保存顶点信息，x表示顶点坐标，y表示距离
         */
        Vector<Point> queue = new Vector<Point>();
        
        int x = root;
        for(int i=0; i<6400; i++) {
            marked[i] = false;
        }
        queue.add(new Point(x, 0));
        while(!queue.isEmpty()){
            Point p = queue.get(0);
            marked[p.x] = true;
            //计算两点之间的距离
            for(int i=1; i<=4; i++) {
                int next =   p.x + offset[i];//方向
                if(next >= 0 && next < 6400 && marked[next] == false && map[p.x][next] == 1) {
                    //计算距离
                    marked[next] = true;
                    queue.add(new Point(next, p.y+1));
                    Dis[x][next] = p.y + 1;
                    Dis[next][x] = p.y + 1;
                    //求出最短路径
                    edgeTo[next] = p.x;
                }
            }
            //退出队列
            queue.remove(0);
        }
    }
    
   
    /**
     *    求出目标到出发点的距离
     */
    public int Dis(int x1, int y1, int x2, int y2) {
        bfs(x1 * 80 + y1);
        return Dis[x1*80 + y1][x2 *80 + y2];
    }
    
    /**
     * 求出最短路径
     */
    public void toPath(int x1, int y1, int x2, int y2, Stack<Integer> path) {
        int beg = (x1 *80 + y1);
        int arr = (x2 * 80 + y2);
        bfs(beg);
        for(int x = arr; x != beg; x = this.edgeTo[x]) {
            path.push(x);
        }
        path.push(beg);
    }
    
    
    @Override
    public void run() {
        this.Simulator();
    }
}
