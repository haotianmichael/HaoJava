package buaaoo;

import java.io.*;

public class Map {

    public static int MAXNUM = 10000000;
    private int[][]map  = new int [80][80];  //地图
    private int[][]mMatrix = new int[6400][6400]; //邻接矩阵
    private static int PRE_LINE = 6400;
    public Map(String filepath){
        try(
                FileReader reader = new FileReader(filepath);
                BufferedReader br = new BufferedReader(reader)) {

            String line;
            int numRow = 0;
            while((line = br.readLine()) != null) {
                if(line.equals("exit")) {
                    System.exit(1);
                }
                if(!IsvalidChar(line.replace(" ", ""), numRow++)){
                    System.exit(0);
                }
            }
            if(numRow < 80) {
                System.out.println("文件输入必须为80行，请重新输入！");
                System.exit(0);
            }
            if(!Ismap()) {
                System.out.println("输入的图文件不是连通图！请重新输入");
                System.exit(0);
            }
        }catch (IOException e) {
            System.out.println("地图文件无效，请重新确定格式！");
            System.exit(0);
        }
    }

    //连通图判断
    public boolean Ismap() {
        boolean is = true;

        //边界条件
        for(int i=0; i<79; i++) {
            if ((map[79][i] == 2) || (map[79][i] == 3)) {
                is = false;
                break;
            }
        }
        if(map[79][79] != 0) {
            is = false;
        }
        for(int i=0; i<79; i++) {
            if((map[i][79] == 1) || (map[i][79] == 3)) {
                is = false;
                break;
            }
        }

        //连通图判断


        return is;
    }

    //格式判断
    public boolean IsvalidChar(String line, int row) {
        boolean is = true;
        if(row >= 80) {
            System.out.println("文件输入必须为80行，请重新输入！");
            is = false;
        }else {
            if (line.length() == 80) {
                for (int i = 0; i < 80; i++) {
                    try {
                        int ins = Integer.parseInt(line.substring(i, i + 1));
                        if (ins >= 0 && ins <= 3) {
                            map[row][i] = ins;
                        } else {
                            throw new NumberFormatException();
                        }
                    } catch (NumberFormatException e) {
                        System.out.println("无效字符！请重新输入！");
                        is = false;
                        break;
                    }
                }
            } else {
                System.out.println("文件输入必须为80列！请重新输入！");
                is = false;
            }
        }
        return is;
    }

    //邻接矩阵初始化
    public void matrixUDG() {
        for(int i=0; i<PRE_LINE; i++) {
            for(int j=0; j<PRE_LINE; j++) {
                if(i == j) {
                    mMatrix[i][i] = 0;
                }else {
                    mMatrix[i][j] = MAXNUM;
                }
            }
        }

        for(int i=0; i<80; i++) {
            for(int j=0; j<80; j++) {
                if(map[i][j] == 1) {
                    mMatrix[i * 80 + j][i * 80 + j + 1] = 1;
                    mMatrix[i * 80 + j + 1][i * 80 + j] = 1;
                }
                if(map[i][j]== 3) {
                    mMatrix[i * 80 + j][i * 80 + j + 1] = 1;
                    mMatrix[i * 80 + j + 1][i * 80 + j] = 1;

                    mMatrix[i * 80 + j][(i + 1) * 80 + j] = 1;
                    mMatrix[(i + 1) * 80 + j][i * 80 + j] = 1;
                }
                if(map[i][j] == 2){
                    mMatrix[i * 80 + j][(i + 1) * 80 + j] = 1;
                    mMatrix[(i + 1) * 80 + j][i * 80 + j] = 1;
                }
            }
        }
    }

    public int[][]getMap(){
        return this.map;
    }

    public int[][]getmMatrix() {
        this.matrixUDG();
        return this.mMatrix;
    }


}
